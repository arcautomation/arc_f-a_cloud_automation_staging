package com.arc.fna.testscripts;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.AdvancedSearchPage;
import com.arc.fna.pages.CommunicationPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.FolderDownloadPage;
import com.arc.fna.pages.GlobalSearchPage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)

public class FNA_AdvancedSearch
{
	
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	AdvancedSearchPage advancedSearchPage;
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser)
	{
		
		if(browser.equalsIgnoreCase("firefox"))
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	
	/** TC_001 (Advanced Search): Verify on searching the content of the file with characters the file show up in search result.  
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (Advanced Search): Verify on searching the content of the file with characters the file show up in search result.")
	public void verifyContent_Search() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_001 (Advanced Search): Verify on searching the content of the file with characters the file show up in search result");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("CollectionAccountteam");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String Collection_Name = PropertyReader.getProperty("SelectCollectionrename11");			
			fnaHomePage.selectcollection(Collection_Name);	
			String FolderName = PropertyReader.getProperty("SelectFoldername12");			
			fnaHomePage.Select_Folder(FolderName);	
			driver.switchTo().defaultContent();
			advancedSearchPage = new AdvancedSearchPage(driver).get();
			String ContentName = PropertyReader.getProperty("Searchkeyword");	
			Log.assertThat(advancedSearchPage.content_search(ContentName), "Content Search Successfull", "Content Search UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
	
	/** TC_002 (Advanced Search): Verify Edit the attributes all selected assign the attributes then preview save search.  
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_002 (Advanced Search): Verify Edit the attributes all selected assign the attributes then preview save search.")
	public void verifyEditAttributes_Preview() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_002 (Advanced Search): Verify Edit the attributes all selected assign the attributes then preview save search.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("CollectionAccountteam");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfully", "Adding folder UnSuccessfully", driver);
			File Path=new File(PropertyReader.getProperty("MultipleFolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.Upload_MultipleFiles(FolderPath), "Multiple files uploaded Successfully", "Multiple files uploaded UnSuccessfully", driver);
         	driver.switchTo().defaultContent();
			advancedSearchPage = new AdvancedSearchPage(driver).get();
			String Documenttype_Name = PropertyReader.getProperty("Documenttype11");
			Log.assertThat(advancedSearchPage.EditAttributes_Preview(Documenttype_Name), "Preview edit attribute files Successfully", "Preview edit attribute files UnSuccessfully", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}		
	
	/** TC_003 (Advanced Search): Verify Search in the modify attributes section then assign the values.  
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_003 (Advanced Search): Verify Search in the modify attributes section then assign the values.")
	public void verifyModufyAttributes_Search() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_003 (Advanced Search): Verify Search in the modify attributes section then assign the values.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("CollectionAccountteam");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfully", "Adding folder UnSuccessfully", driver);
			File Path=new File(PropertyReader.getProperty("MultipleFolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.Upload_MultipleFiles(FolderPath), "Multiple files uploaded Successfully", "Multiple files uploaded UnSuccessfully", driver);
        	driver.switchTo().defaultContent();
			advancedSearchPage = new AdvancedSearchPage(driver).get();
			String Documenttype_Name = PropertyReader.getProperty("Documenttype11");
			Log.assertThat(advancedSearchPage.ModifyAttributes_Search(Documenttype_Name), "Modify Attributes Search Successfully", "Modify Attributes Search UnSuccessfully", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
	
	/** TC_004 (Advanced Search): Verify root folder select then search sub folder results should populate.  
	  *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 3, enabled = true, description = "TC_004 (Advanced Search): Verify root folder select then search subfolder results should populate.")
	public void verifySubfolder_searchresults() throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_004 (Advanced Search): Verify root folder select then search subfolder results should populate.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("CollectionAccountteam");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();								
			fnaHomePage.Create_Collections(CollectionName);	
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfully", "Adding folder UnSuccessfully", driver);
			String SubFolderName= fnaHomePage.Random_SubFoldername();	
			Log.assertThat(fnaHomePage.Adding_SubFolder(SubFolderName), "Adding Subfolder Successfull", "Adding Subfolder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("MultipleFolderPath"));			
			String FolderPath = Path.getAbsolutePath().toString();
			Log.assertThat(fnaHomePage.Upload_MultipleFiles(FolderPath), "Multiple files uploaded Successfully", "Multiple files uploaded UnSuccessfully", driver);
			
			driver.switchTo().defaultContent();
			advancedSearchPage = new AdvancedSearchPage(driver).get();			
			String Documenttype_Name = PropertyReader.getProperty("Documenttype12");			
			Log.assertThat(advancedSearchPage.Subfolder_searchresults(SubFolderName,Documenttype_Name), "Select root folder search Successfully", "Select root folder search UnSuccessfully", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
	
}

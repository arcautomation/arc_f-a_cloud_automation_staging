package com.arc.fna.testscripts;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.FolderDownloadPage;
import com.arc.fna.pages.GlobalSearchPage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)

public class FNA_FolderDownload
{
	
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	FolderDownloadPage folderDownloadPage;
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser)
	{
		
		if(browser.equalsIgnoreCase("firefox"))
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	
	/** TC_001 (FolderDownload): Verify User move up and move down button on folder level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (FolderDownload): Verify User move up and move down button on folder level.")
	public void verifyCreateCollection_SelectedUploadFolder () throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_001 (FolderDownload): Verify User move up and move down button on folder level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String CollectionName = PropertyReader.getProperty("SelectCollectionMoveup");						
			fnaHomePage.selectcollection(CollectionName);			
			driver.switchTo().defaultContent();
			folderDownloadPage = new FolderDownloadPage(driver).get();			
			Log.assertThat(folderDownloadPage.Moveup_Movedown_Folderlevel(), "moveup and movedown successfull with valid credential", "moveup and movedown unsuccessfull with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_002 (FolderDownload): Verify folder rename after download from folder level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_002 (FolderDownload): Verify folder rename after download from folder level.")
	public void verifyRenameFolder_Download () throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_002 (FolderDownload): Verify folder rename after download from folder level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String CollectionName = PropertyReader.getProperty("SelectCollectionrename");						
			fnaHomePage.selectcollection(CollectionName);			
			driver.switchTo().defaultContent();
			folderDownloadPage = new FolderDownloadPage(driver).get();
			String FolderName= folderDownloadPage.Random_Foldername();				
			Log.assertThat(folderDownloadPage.Adding_Rename_Folder(FolderName), "Adding rename folder Successfull", "Adding rename folder UnSuccessful", driver);
			String usernamedir=System.getProperty("user.name");
            String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads"; 	
			Log.assertThat(folderDownloadPage.Download_Folder(DownloadPath,FolderName), "Download rename folder Successfull", "Download rename folder UnSuccessful", driver);
		
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_003 (FolderDownload): Verify create same folder name from folder level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_003 (FolderDownload): Verify create same folder name from folder level.")
	public void verifySameFoldername () throws Exception
	{
		try
		{			
			Log.testCaseInfo("TC_003 (FolderDownload): Verify create same folder name from folder level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String CollectionName = PropertyReader.getProperty("SelectSamename");						
			fnaHomePage.selectcollection(CollectionName);			
			driver.switchTo().defaultContent();
			folderDownloadPage = new FolderDownloadPage(driver).get();						
			String FolderName = PropertyReader.getProperty("SelectFoldername12");	
			Log.assertThat(folderDownloadPage.Same_Foldername(FolderName), "Same Folder name Successfull", "Same Folder name UnSuccessful", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
}
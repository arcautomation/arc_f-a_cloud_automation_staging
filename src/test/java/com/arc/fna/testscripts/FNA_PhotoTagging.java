package com.arc.fna.testscripts;

import atu.testrecorder.exceptions.ATUTestRecorderException;
import com.arc.fna.pages.*;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.ViewerUtils;
import com.arcautoframe.utils.Log;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.fail;

public class FNA_PhotoTagging {

    static WebDriver driver;
    LoginPage loginPage;
    HomePage homePage;
    CollectionsPage collectionsPage;
    DocumentsPage documentsPage;
    ViewerPage viewerPage;

    @Parameters("browser")
    @BeforeMethod
    public WebDriver beforeTest(String browser) throws ATUTestRecorderException {
        if (browser.equalsIgnoreCase("firefox")) {
            File dest = new File("./drivers/win/geckodriver.exe");
            // System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
            System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
            driver = new FirefoxDriver();
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        else if (browser.equalsIgnoreCase("chrome")) {
            File dest = new File("./drivers/win/chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("download.default_directory", "C:" + File.separator + "Users" + File.separator
                    + System.getProperty("user.name") + File.separator + "Downloads");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            options.setExperimentalOption("prefs", prefs);
            driver = new ChromeDriver(options);
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        else if (browser.equalsIgnoreCase("safari")) {
            System.setProperty("webdriver.safari.noinstall", "true"); // To stop uninstall each time
            driver = new SafariDriver();
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        return driver;
    }

    /**
     * TC_065 (Viewer): Verify whether the Search Tag Type Field is functioning properly in the Viewer
     *
     * @throws Exception Modified By Arka Halder | 08-January-2019
     *
     */
     @Test(priority = 1, enabled = true, description = "TC_065 (Viewer): Verify whether the Search Tag Type Field is functioning properly in the Viewer")
    public void verifySearchFunctionalityInTagCreationForm() throws Exception {
        Screen screen = new Screen();
        try {
            Log.testCaseInfo("TC_065 (Viewer): Verify whether the Search Tag Type Field is functioning properly in the Viewer");
            String loginEmail = PropertyReader.getProperty("loginEmail");
            String loginPassword = PropertyReader.getProperty("loginPassword");
            String collectionName = PropertyReader.getProperty("collectionName");
            String directoryName = PropertyReader.getProperty("directoryName");
            String documentName = PropertyReader.getProperty("documentName");
            String photoTaggingFolder = System.getProperty("user.dir") + PropertyReader.getProperty("photoTaggingFolder");
            String photoTaggingFile = PropertyReader.getProperty("photoTaggingFile");
            photoTaggingFile = photoTaggingFolder + photoTaggingFile;
            loginPage = new LoginPage(driver).get();
            Log.message("Skysite Login Page has been opened.");
            homePage = loginPage.performLoginOperation(loginEmail, loginPassword);
            Log.message("User has been logged in successfully.");
            collectionsPage = homePage.openCollectionsTab();
            Log.message("User is now in the Collections Tab.");
            documentsPage = collectionsPage.openCollection(collectionName);
            Log.message("User has selected the intended Collection.");
            ArrayList<String> directoryPath = new ArrayList<String>();
            directoryPath.add(collectionName);
            directoryPath.add(directoryName);
            documentsPage.openCollectionDirectory(directoryPath);
            Log.message("User is now in the Documents Tab.");
            viewerPage = documentsPage.openDocumentInViewer(documentName);
            Log.message("User has opened the intended document in Viewer.");
            viewerPage.clickOnPhotoTaggingAnnotation();
            Log.message("User has picked the Tagging Annotation up from the menu.");
            Pattern pattern = new Pattern(photoTaggingFile);
            screen.click(pattern);
            Log.message("User has clicked on a location over the document.");
            viewerPage.clickOnPhotoTaggingSelectIconButton();
            Log.message("User has opened the Dropdown to mention the Equipment type.");
            viewerPage.typeInPhotoTaggingIconDropdownSearch("Condenser");
            Log.message("User has entered \'Condenser\' in the Search Tag Type Fields.");
            Assert.assertTrue(viewerPage.isPhotoTaggingTypePresentInList("Condenser", true));
        }
        catch (Exception e) {
            e.getCause();
            Log.exception(e, driver);
        }
        finally {
            screen.click(screen.getTarget());
            Log.endTestCase();
            driver.quit();
        }
    }

    /**
     * TC_064 (Viewer): Verify whether the Tag Type can be changed in the Viewer
     *
     * @throws Exception Modified By Arka Halder | 08-January-2019
     *
     */
    @Test(priority = 2, enabled = true, description = "TC_064 (Viewer): Verify whether the Tag Type can be changed properly in the Viewer")
    public void verifyChangeOfIconsOfATagInTagCreationForm() throws Exception {
        Screen screen = new Screen();
        try {
            Log.testCaseInfo("TC_064 (Viewer): Verify whether the Tag Type can be changed in the Viewer");
            String loginEmail = PropertyReader.getProperty("loginEmail");
            String loginPassword = PropertyReader.getProperty("loginPassword");
            String collectionName = PropertyReader.getProperty("collectionName");
            String directoryName = PropertyReader.getProperty("directoryName");
            String documentName = PropertyReader.getProperty("documentName");
            String photoTaggingFolder = System.getProperty("user.dir") + PropertyReader.getProperty("photoTaggingFolder");
            String photoTaggingFile = PropertyReader.getProperty("photoTaggingFile");
            photoTaggingFile = photoTaggingFolder + photoTaggingFile;
            loginPage = new LoginPage(driver).get();
            Log.message("Skysite Login Page has been opened.");
            homePage = loginPage.performLoginOperation(loginEmail, loginPassword);
            Log.message("User has been logged in successfully.");
            collectionsPage = homePage.openCollectionsTab();
            Log.message("User is now in the Collections Tab.");
            documentsPage = collectionsPage.openCollection(collectionName);
            Log.message("User has selected the intended Collection.");
            ArrayList<String> directoryPath = new ArrayList<String>();
            directoryPath.add(collectionName);
            directoryPath.add(directoryName);
            documentsPage.openCollectionDirectory(directoryPath);
            Log.message("User is now in the Documents Tab.");
            viewerPage = documentsPage.openDocumentInViewer(documentName);
            Log.message("User has opened the intended document in Viewer.");
            viewerPage.clickOnPhotoTaggingAnnotation();
            Log.message("User has picked the Tagging Annotation up from the menu.");
            Pattern pattern = new Pattern(photoTaggingFile);
            screen.click(pattern);
            Log.message("User has clicked on a location over the document.");
            viewerPage.clickOnPhotoTaggingSelectIconButton();
            Log.message("User has opened the Dropdown to mention the Equipment type.");
            viewerPage.selectIntendedPhotoTaggingIcon("Fire Extinguisher");
            Log.message("User has selected the intended Equipment type.");
            Assert.assertEquals(viewerPage.getSelectedPhotoTaggingIconText(), "Fire Extinguisher");
            Log.message("The Dropdown is set to the selected Equipment type.");
            viewerPage.clickOnPhotoTaggingSelectIconButton();
            Log.message("User has decided to change the selected Equipment type and opens the Dropdown again.");
            viewerPage.selectIntendedPhotoTaggingIcon("Fire Hose");
            Log.message("User has selected the intended Equipment type.");
            Assert.assertEquals(viewerPage.getSelectedPhotoTaggingIconText(), "Fire Hose");
        }
        catch (Exception e) {
            e.getCause();
            Log.exception(e, driver);
        }
        finally {
            screen.click(screen.getTarget());
            Log.endTestCase();
            driver.quit();
        }
    }

    /**
     * TC_066 (Viewer): Verify whether Photo Tags are getting created and deleted properly in the Viewer
     *
     * @throws Exception Modified By Arka Halder | 08-January-2019
     *
     */
    @Test(priority = 3, enabled = true, description = "TC_066 (Viewer): Verify whether Photo Tags are getting created and deleted properly in the Viewer")
    public void verifyCreationAndDeletionOfATag() throws Exception {
        Screen screen = new Screen();
        try {
            Log.testCaseInfo("TC_066 (Viewer): Verify whether Photo Tags are getting created and deleted properly in the Viewer");
            String loginEmail = PropertyReader.getProperty("loginEmail");
            String loginPassword = PropertyReader.getProperty("loginPassword");
            String collectionName = PropertyReader.getProperty("collectionName");
            String directoryName = PropertyReader.getProperty("directoryName");
            String documentName = PropertyReader.getProperty("documentName");
            String photoTaggingFolder = System.getProperty("user.dir") + PropertyReader.getProperty("photoTaggingFolder");
            String photoTaggingFile = PropertyReader.getProperty("photoTaggingFile");
            photoTaggingFile = photoTaggingFolder + photoTaggingFile;
            loginPage = new LoginPage(driver).get();
            Log.message("Skysite Login Page has been opened.");
            homePage = loginPage.performLoginOperation(loginEmail, loginPassword);
            Log.message("User has been logged in successfully.");
            collectionsPage = homePage.openCollectionsTab();
            Log.message("User is now in the Collections Tab.");
            documentsPage = collectionsPage.openCollection(collectionName);
            Log.message("User has selected the intended Collection.");
            ArrayList<String> directoryPath = new ArrayList<String>();
            directoryPath.add(collectionName);
            directoryPath.add(directoryName);
            documentsPage.openCollectionDirectory(directoryPath);
            Log.message("User is now in the Documents Tab.");
            viewerPage = documentsPage.openDocumentInViewer(documentName);
            Log.message("User has opened the intended document in Viewer.");

            String screenShotA = photoTaggingFolder + "screenShotA.png";
            File screenShotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenShotFile, new File(screenShotA));
            Log.message("A screenshot has been captured.");

            viewerPage.clickOnPhotoTaggingAnnotation();
            Log.message("User has picked the Tagging Annotation up from the menu.");
            Pattern pattern = new Pattern(photoTaggingFile);
            screen.click(pattern);
            Log.message("User has clicked on a location over the document.");
            viewerPage.clickOnPhotoTaggingSelectIconButton();
            Log.message("User has opened the Dropdown to mention the Equipment type.");
            viewerPage.selectIntendedPhotoTaggingIcon("Network");
            Log.message("User has selected the intended Equipment type.");
            viewerPage.typeInPhotoTaggingDescriptionField("This is the Description of an Network.");
            Log.message("User has provided some text in the Description Field.");

            viewerPage.clickOnEquipmentAttachButton();
            viewerPage.selectIntendedEquipmentFromList("Router_7th_Floor");
            viewerPage.clickOnPhotoTaggingEquipmentDoneButton();

            viewerPage.typeInPhotoTaggingNoteField("This is the Note of an Network.");
            Log.message("User has provided some text in the Note Field.");
            viewerPage.clickOnPhotoTaggingCreateButton();
            Log.message("The Tag has been created successfully.");

            String screenShotB = photoTaggingFolder + "screenShotB.png";
            screenShotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenShotFile, new File(screenShotB));
            Log.message("A screenshot has been captured.");

            viewerPage.clickOnTagsAccumulatorLink();
            viewerPage.toggleVisibilityOfTheTag(); // no change. need to consult.
            viewerPage.clickOnTagsAccumulatorLink();
            Log.message("User has opened the Tags Accumulator menu.");
            viewerPage.toggleVisibilityOfTheTag(); // becomes invisible
            Log.message("The Tag is invisible now.");
            viewerPage.clickOnTagsAccumulatorLink();
            Log.message("User has opened the Tags Accumulator menu.");
            viewerPage.toggleVisibilityOfTheTag(); // becomes visible
            Log.message("The Tag is visible again.");

            String screenShotC = photoTaggingFolder + "screenShotC.png";
            screenShotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenShotFile, new File(screenShotC));
            Log.message("A screenshot has been captured.");

            screen.click();
            Log.message("User has opened the Tag Edit modal.");
            viewerPage.clickOnPhotoTaggingDeleteButton();
            Log.message("User has clicked on the Delete Tag button.");
            viewerPage.clickOnPhotoTaggingDeleteYesButton();
            Log.message("The Tag has been deleted successfully.");

            String screenShotD = photoTaggingFolder + "screenShotD.png";
            screenShotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenShotFile, new File(screenShotD));
            Log.message("A screenshot has been captured.");

            try {
                //Assert.assertTrue(ViewerUtils.compareScreenShots(screenShotA, screenShotD));
                Log.message("Comparison between the captured screenshots is being launched.");
                Assert.assertTrue(ViewerUtils.compareScreenShots(screenShotB, screenShotC));
                Log.message("Comparison between the captured screenshots is successful.");
            }
            catch(AssertionError e) {
                fail();
            }
            finally {
                new File(screenShotA).delete();
                new File(screenShotB).delete();
                new File(screenShotC).delete();
                new File(screenShotD).delete();
                Log.message("All Screenshots have been deleted successfully.");
            }
        }
        catch (Exception e) {
            e.getCause();
            Log.exception(e, driver);
        }
        finally {
            screen.click(screen.getTarget());
            Log.endTestCase();
            driver.quit();
        }
    }

    /**
     * TC_067 (Viewer): Verify whether Photo Tags are can be created with a very long description
     *
     * @throws Exception Modified By Arka Halder | 09-January-2019
     *
     */
    @Test(priority = 4, enabled = true, description = "TC_067 (Viewer): Verify whether Photo Tags are can be created with a very long description")
    public void verifyCreationAndDeletionOfATagWithVeryLongDescription() throws Exception {
        Screen screen = new Screen();
        try {
            Log.testCaseInfo("TC_067 (Viewer): VVerify whether Photo Tags are can be created with a very long description");
            String loginEmail = PropertyReader.getProperty("loginEmail");
            String loginPassword = PropertyReader.getProperty("loginPassword");
            String collectionName = PropertyReader.getProperty("collectionName");
            String directoryName = PropertyReader.getProperty("directoryName");
            String documentName = PropertyReader.getProperty("documentName");
            String photoTaggingFolder = System.getProperty("user.dir") + PropertyReader.getProperty("photoTaggingFolder");
            String photoTaggingFile = PropertyReader.getProperty("photoTaggingFile");
            photoTaggingFile = photoTaggingFolder + photoTaggingFile;
            loginPage = new LoginPage(driver).get();
            Log.message("Skysite Login Page has been opened.");
            homePage = loginPage.performLoginOperation(loginEmail, loginPassword);
            Log.message("User has been logged in successfully.");
            collectionsPage = homePage.openCollectionsTab();
            Log.message("User is now in the Collections Tab.");
            documentsPage = collectionsPage.openCollection(collectionName);
            Log.message("User has selected the intended Collection.");
            ArrayList<String> directoryPath = new ArrayList<String>();
            directoryPath.add(collectionName);
            directoryPath.add(directoryName);
            documentsPage.openCollectionDirectory(directoryPath);
            Log.message("User is now in the Documents Tab.");
            viewerPage = documentsPage.openDocumentInViewer(documentName);
            Log.message("User has opened the intended document in Viewer.");
            viewerPage.clickOnPhotoTaggingAnnotation();
            Log.message("User has picked the Tagging Annotation up from the menu.");
            Pattern pattern = new Pattern(photoTaggingFile);
            screen.click(pattern);
            Log.message("User has clicked on a location over the document.");
            viewerPage.clickOnPhotoTaggingSelectIconButton();
            Log.message("User has opened the Dropdown to mention the Equipment type.");
            viewerPage.selectIntendedPhotoTaggingIcon("Fire Extinguisher");
            Log.message("User has selected the intended Equipment type.");
            String randomString = RandomStringUtils.randomAlphabetic(1001);
            Log.message("User is typing Description = " + randomString);
            viewerPage.typeInPhotoTaggingDescriptionField(randomString);
            Log.message("User has entered a description, which is " + randomString.length() + " characters long.");

            // ** The following part of the script will be changed ** //
            viewerPage.clickOnPhotoTaggingCreateButton();
            Log.message("The Tag has been created successfully.");
            screen.click();
            Log.message("User has opened the Tag Edit modal.");
            viewerPage.clickOnPhotoTaggingDeleteButton();
            Log.message("User has clicked on the Delete Tag button.");
            viewerPage.clickOnPhotoTaggingDeleteYesButton();
            Log.message("The Tag has been deleted successfully.");
        }
        catch (Exception e) {
            e.getCause();
            Log.exception(e, driver);
        }
        finally {
            screen.click(screen.getTarget());
            Log.endTestCase();
            driver.quit();
        }
    }

    /**
     * TC_068 (Viewer): Verify whether Photo Tags are can be created with a description containing special characters
     *
     * @throws Exception Modified By Arka Halder | 10-January-2019
     *
     */
    @Test(priority = 5, enabled = true, description = "TC_068 (Viewer): Verify whether Photo Tags are can be created with a description containing special characters")
    public void verifyCreationAndDeletionOfATagWithDescriptionContainingSpecialCharacters() throws Exception {
        Screen screen = new Screen();
        try {
            Log.testCaseInfo("TC_068 (Viewer): Verify whether Photo Tags are can be created with a description containing special characters");
            String loginEmail = PropertyReader.getProperty("loginEmail");
            String loginPassword = PropertyReader.getProperty("loginPassword");
            String collectionName = PropertyReader.getProperty("collectionName");
            String directoryName = PropertyReader.getProperty("directoryName");
            String documentName = PropertyReader.getProperty("documentName");
            String photoTaggingFolder = System.getProperty("user.dir") + PropertyReader.getProperty("photoTaggingFolder");
            String photoTaggingFile = PropertyReader.getProperty("photoTaggingFile");
            photoTaggingFile = photoTaggingFolder + photoTaggingFile;
            loginPage = new LoginPage(driver).get();
            Log.message("Skysite Login Page has been opened.");
            homePage = loginPage.performLoginOperation(loginEmail, loginPassword);
            Log.message("User has been logged in successfully.");
            collectionsPage = homePage.openCollectionsTab();
            Log.message("User is now in the Collections Tab.");
            documentsPage = collectionsPage.openCollection(collectionName);
            Log.message("User has selected the intended Collection.");
            ArrayList<String> directoryPath = new ArrayList<String>();
            directoryPath.add(collectionName);
            directoryPath.add(directoryName);
            documentsPage.openCollectionDirectory(directoryPath);
            Log.message("User is now in the Documents Tab.");
            viewerPage = documentsPage.openDocumentInViewer(documentName);
            Log.message("User has opened the intended document in Viewer.");
            viewerPage.clickOnPhotoTaggingAnnotation();
            Log.message("User has picked the Tagging Annotation up from the menu.");
            Pattern pattern = new Pattern(photoTaggingFile);
            screen.click(pattern);
            Log.message("User has clicked on a location over the document.");
            viewerPage.clickOnPhotoTaggingSelectIconButton();
            Log.message("User has opened the Dropdown to mention the Equipment type.");
            viewerPage.selectIntendedPhotoTaggingIcon("Fire Extinguisher");
            Log.message("User has selected the intended Equipment type.");
            viewerPage.typeInPhotoTaggingDescriptionField("Special characters, viz. ~`!@#$%^&*()_-+={[}]\\|:;\"\'<,>.?/");
            Log.message("User has entered a description, which contains special characters.");

            // ** The following part of the script will be changed ** //
            viewerPage.clickOnPhotoTaggingCreateButton();
            Log.message("The Tag has been created successfully.");
            screen.click();
            Log.message("User has opened the Tag Edit modal.");
            viewerPage.clickOnPhotoTaggingDeleteButton();
            Log.message("User has clicked on the Delete Tag button.");
            viewerPage.clickOnPhotoTaggingDeleteYesButton();
            Log.message("The Tag has been deleted successfully.");
        }
        catch (Exception e) {
            e.getCause();
            Log.exception(e, driver);
        }
        finally {
            screen.click(screen.getTarget());
            Log.endTestCase();
            driver.quit();
        }
    }

    /**
     * TC_069 (Viewer): Verify whether Photo Tags are can be created with a very long note
     *
     * @throws Exception Modified By Arka Halder | 10-January-2019
     *
     */
    @Test(priority = 6, enabled = true, description = "TC_069 (Viewer): Verify whether Photo Tags are can be created with a very long note")
    public void verifyCreationAndDeletionOfATagWithVeryLongNote() throws Exception {
        Screen screen = new Screen();
        try {
            Log.testCaseInfo("TC_069 (Viewer): Verify whether Photo Tags are can be created with a very long note");
            String loginEmail = PropertyReader.getProperty("loginEmail");
            String loginPassword = PropertyReader.getProperty("loginPassword");
            String collectionName = PropertyReader.getProperty("collectionName");
            String directoryName = PropertyReader.getProperty("directoryName");
            String documentName = PropertyReader.getProperty("documentName");
            String photoTaggingFolder = System.getProperty("user.dir") + PropertyReader.getProperty("photoTaggingFolder");
            String photoTaggingFile = PropertyReader.getProperty("photoTaggingFile");
            photoTaggingFile = photoTaggingFolder + photoTaggingFile;
            loginPage = new LoginPage(driver).get();
            Log.message("Skysite Login Page has been opened.");
            homePage = loginPage.performLoginOperation(loginEmail, loginPassword);
            Log.message("User has been logged in successfully.");
            collectionsPage = homePage.openCollectionsTab();
            Log.message("User is now in the Collections Tab.");
            documentsPage = collectionsPage.openCollection(collectionName);
            Log.message("User has selected the intended Collection.");
            ArrayList<String> directoryPath = new ArrayList<String>();
            directoryPath.add(collectionName);
            directoryPath.add(directoryName);
            documentsPage.openCollectionDirectory(directoryPath);
            Log.message("User is now in the Documents Tab.");
            viewerPage = documentsPage.openDocumentInViewer(documentName);
            Log.message("User has opened the intended document in Viewer.");
            viewerPage.clickOnPhotoTaggingAnnotation();
            Log.message("User has picked the Tagging Annotation up from the menu.");
            Pattern pattern = new Pattern(photoTaggingFile);
            screen.click(pattern);
            Log.message("User has clicked on a location over the document.");
            viewerPage.clickOnPhotoTaggingSelectIconButton();
            Log.message("User has opened the Dropdown to mention the Equipment type.");
            viewerPage.selectIntendedPhotoTaggingIcon("Fire Extinguisher");
            Log.message("User has selected the intended Equipment type.");
            String randomString = RandomStringUtils.randomAlphabetic(1001);
            Log.message("User is typing Note = " + randomString);
            viewerPage.typeInPhotoTaggingNoteField(randomString);
            Log.message("User has entered a note, which is " + randomString.length() + " characters long.");

            // ** The following part of the script will be changed ** //
            viewerPage.clickOnPhotoTaggingCreateButton();
            Log.message("The Tag has been created successfully.");
            screen.click();
            Log.message("User has opened the Tag Edit modal.");
            viewerPage.clickOnPhotoTaggingDeleteButton();
            Log.message("User has clicked on the Delete Tag button.");
            viewerPage.clickOnPhotoTaggingDeleteYesButton();
            Log.message("The Tag has been deleted successfully.");
        }
        catch (Exception e) {
            e.getCause();
            Log.exception(e, driver);
        }
        finally {
            screen.click(screen.getTarget());
            Log.endTestCase();
            driver.quit();
        }
    }

    /**
     * TC_070 (Viewer): Verify whether Photo Tags are can be created with a note containing special characters
     *
     * @throws Exception Modified By Arka Halder | 10-January-2019
     *
     */
    @Test(priority = 7, enabled = true, description = "TC_070 (Viewer): Verify whether Photo Tags are can be created with a note containing special characters")
    public void verifyCreationAndDeletionOfATagWithNoteContainingSpecialCharacters() throws Exception {
        Screen screen = new Screen();
        try {
            Log.testCaseInfo("TC_070 (Viewer): Verify whether Photo Tags are can be created with a note containing special characters");
            String loginEmail = PropertyReader.getProperty("loginEmail");
            String loginPassword = PropertyReader.getProperty("loginPassword");
            String collectionName = PropertyReader.getProperty("collectionName");
            String directoryName = PropertyReader.getProperty("directoryName");
            String documentName = PropertyReader.getProperty("documentName");
            String photoTaggingFolder = System.getProperty("user.dir") + PropertyReader.getProperty("photoTaggingFolder");
            String photoTaggingFile = PropertyReader.getProperty("photoTaggingFile");
            photoTaggingFile = photoTaggingFolder + photoTaggingFile;
            loginPage = new LoginPage(driver).get();
            Log.message("Skysite Login Page has been opened.");
            homePage = loginPage.performLoginOperation(loginEmail, loginPassword);
            Log.message("User has been logged in successfully.");
            collectionsPage = homePage.openCollectionsTab();
            Log.message("User is now in the Collections Tab.");
            documentsPage = collectionsPage.openCollection(collectionName);
            Log.message("User has selected the intended Collection.");
            ArrayList<String> directoryPath = new ArrayList<String>();
            directoryPath.add(collectionName);
            directoryPath.add(directoryName);
            documentsPage.openCollectionDirectory(directoryPath);
            Log.message("User is now in the Documents Tab.");
            viewerPage = documentsPage.openDocumentInViewer(documentName);
            Log.message("User has opened the intended document in Viewer.");
            viewerPage.clickOnPhotoTaggingAnnotation();
            Log.message("User has picked the Tagging Annotation up from the menu.");
            Pattern pattern = new Pattern(photoTaggingFile);
            screen.click(pattern);
            Log.message("User has clicked on a location over the document.");
            viewerPage.clickOnPhotoTaggingSelectIconButton();
            Log.message("User has opened the Dropdown to mention the Equipment type.");
            viewerPage.selectIntendedPhotoTaggingIcon("Fire Extinguisher");
            Log.message("User has selected the intended Equipment type.");
            viewerPage.typeInPhotoTaggingNoteField("Special characters, viz. ~`!@#$%^&*()_-+={[}]\\|:;\"\'<,>.?/");
            Log.message("User has entered a note, which contains special characters.");

            // ** The following part of the script will be changed ** //
            viewerPage.clickOnPhotoTaggingCreateButton();
            Log.message("The Tag has been created successfully.");
            screen.click();
            Log.message("User has opened the Tag Edit modal.");
            viewerPage.clickOnPhotoTaggingDeleteButton();
            Log.message("User has clicked on the Delete Tag button.");
            viewerPage.clickOnPhotoTaggingDeleteYesButton();
            Log.message("The Tag has been deleted successfully.");
        }
        catch (Exception e) {
            e.getCause();
            Log.exception(e, driver);
        }
        finally {
            screen.click(screen.getTarget());
            Log.endTestCase();
            driver.quit();
        }
    }

}
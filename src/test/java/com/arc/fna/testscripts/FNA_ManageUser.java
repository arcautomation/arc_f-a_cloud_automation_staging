package com.arc.fna.testscripts;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;

import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;


@Listeners(EmailReport.class)

public class FNA_ManageUser {
	
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	
	@Parameters("browser")
	@BeforeMethod
	public WebDriver beforeTest(String browser) {
		
		if(browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	}
		else if (browser.equalsIgnoreCase("chrome")) { 
		File dest = new File("./drivers/win/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver( options );
		driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
	  } 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
		
	/** TC_001 (Manage User): Verify Usage fields showing proper data..
	 * Scripted By: Sekhar 
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (Manage User): Verify Usage fields showing proper data.")
	public void verifyLandingOfManageUserPage() throws Exception
	{
		try
		{
			Log.testCaseInfo("TC_001 (Manage User): Verify Usage fields showing proper data.");
			loginPage = new LoginPage(driver).get();			
			String uName = PropertyReader.getProperty("ManageUsername");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			Log.assertThat(fnaHomePage.openManageUser(), "Usage fields of manage User Page Successful with valid credential", "Usage fields of manage User Page unsuccessful with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_002 (Manage User): Verify total number of users in user list count should match as used seat count.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	
	@Test(priority = 1, enabled = true, description = "TC_002 (Manage User): Verify total number of users in user list count should match as used seat count.")
	public void verifyUsedData() throws Exception 
	{
		try
		{
			Log.testCaseInfo("TC_002 (Manage User): Verify total number of users in user list count should match as used seat count.");
			loginPage = new LoginPage(driver).get();
			String uName = PropertyReader.getProperty("ManageUsername");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);
			fnaHomePage.loginValidation();
			Log.assertThat(fnaHomePage.verifyUsedCount(), "Total number of user in Usage count and in table are same", "Total number of user in Usage count and in table are not same", driver);;
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_003 (Manage User): Verify grid search of manage user page.
      * Scripted By: Sekhar
      * @throws Exception
     *//*
     @Test(priority = 2, enabled = true, description = "TC_003 (Manage User): Verify grid search of manage user page")
     public void verifyGridSearchManageUserPage() throws Exception
     {
            try
            {
                   Log.testCaseInfo("TC_003 (Manage User): Verify grid search of manage user page");
                   loginPage = new LoginPage(driver).get();                
                   String uName = PropertyReader.getProperty("ManageUsername");
       		   	   String pWord = PropertyReader.getProperty("Password");			
       		   	   fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
       		   	   fnaHomePage.loginValidation();
                   Log.assertThat(fnaHomePage.gridSearchWithUserNameManageUser(), "Grid search with user name of manage User Page Successful with valid credential", "Grid search with user name of manage User Page unsuccessful with valid credential", driver);
                   Log.assertThat(fnaHomePage.gridSearchWithMailManageUser(), "Grid search with mail of manage User Page Successful with valid credential", "Grid search with mail of manage User Page unsuccessful with valid credential", driver);
                   Log.assertThat(fnaHomePage.gridSearchWithPhoneManageUser(), "Grid search with Phone work of manage User Page Successful with valid credential", "Grid search with Phone work of manage User Page unsuccessful with valid credential", driver);
            }
            catch(Exception e)
            {
            	AnaylizeLog.analyzeLog(driver);
            	e.getCause();
            	Log.exception(e, driver);
            }
            finally
            {
            	Log.endTestCase();
            	driver.quit();
            }
     }      

     *//** TC_004 (Manage User): Verify grid search using user license of manage user page.
      *  Scripted By: Sekhar
       * @throws Exception
      *//*
      @Test(priority = 3, enabled = true, description = "Verify grid search using user license of manage user page")
      public void verifyGridSearchUserLicenseManageUserPage() throws Exception
      {
             try
             {
                    Log.testCaseInfo("TC_004 (Manage User): Verify grid search using user license of manage user page");
                    loginPage = new LoginPage(driver).get();                
                    String uName = PropertyReader.getProperty("ManageUsername");
        			String pWord = PropertyReader.getProperty("Password");			
        			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord); 
        			fnaHomePage.loginValidation();
                    Log.assertThat(fnaHomePage.gridSearch_UserLicense_LiteUser_ManageUser(), "Grid search using user license with lite user of manage User Page Successful with valid credential", "Grid search using user license with lite user of manage User Page unsuccessful with valid credential", driver);
                    Log.assertThat(fnaHomePage.gridSearch_UserLicense_SharedUser_ManageUser(), "Grid search using user license with shared user of manage User Page Successful with valid credential", "Grid search using user license with shared user of manage User Page unsuccessful with valid credential", driver);
                    Log.assertThat(fnaHomePage.gridSearch_UserLicense_Employee_ManageUser(), "Grid search using user license with employeee of manage User Page Successful with valid credential", "Grid search using user license with employee of manage User Page unsuccessful with valid credential", driver);
             }
             catch(Exception e)
             {
            	 AnaylizeLog.analyzeLog(driver);
                    e.getCause();
                    Log.exception(e, driver);
             }
             finally
             {
                    Log.endTestCase();
                    driver.quit();
             }
      }
      
      *//** TC_005 (Manage User): Verify search reset button of manage user page.
       *  Scripted By: Sekhar
        * @throws Exception
       *//*
       @Test(priority = 4, enabled = true, description = "TC_005 (Manage User): Verify search reset button of manage user page")
       public void verifyResetInGridSearch() throws Exception
       {
              try
              {
                    Log.testCaseInfo("TC_005 (Manage User): Verify search reset button of manage user page");
                    loginPage = new LoginPage(driver).get();                
                    String uName = PropertyReader.getProperty("ManageUsername");
         			String pWord = PropertyReader.getProperty("Password");			
         			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord); 
         			fnaHomePage.loginValidation();
         			Log.assertThat(fnaHomePage.search_Reset_ManageUser(), "Search reset button of manage User Page Successful with valid credential", "Search reset button of manage User Page unsuccessful with valid credential", driver);
              }
              catch(Exception e)
              {
            	  AnaylizeLog.analyzeLog(driver);
                     e.getCause();
                     Log.exception(e, driver);
              }
              finally
              {
                     Log.endTestCase();
                     driver.quit();
              }
       }*/

       /** TC_006 (Manage User): Verify user sorting of manage user page.
         * Scripted By: Sekhar
         * @throws Exception
         */
        @Test(priority = 5, enabled = true, description = "TC_006 (Manage User): Verify user sorting of manage user page")
        public void verifyUserSortingManageUserPage() throws Exception
        {
               try
               {
                      Log.testCaseInfo("TC_006 (Manage User): Verify user sorting of manage user page");
                      loginPage = new LoginPage(driver).get();                
                      String uName = PropertyReader.getProperty("ManageUsername");
          			String pWord = PropertyReader.getProperty("Password");			
          			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);   
          			fnaHomePage.loginValidation();
          			Log.assertThat(fnaHomePage.user_Sort_ManageUser(), "User sorting of manage User Page Successful with valid credential", "User sorting of manage User Page unsuccessful with valid credential", driver);
               }
               catch(Exception e)
               {
            	   AnaylizeLog.analyzeLog(driver);
                      e.getCause();
                      Log.exception(e, driver);
               }
               finally
               {
                      Log.endTestCase();
                      driver.quit();
               }
        }
        
        
        /** TC_007 (Manage User): Verify shared user of manage user 
         * 	Scripted By: Sekhar
         * @throws Exception
        */
        @Test(priority = 6, enabled = true, description = "TC_007 (Manage User): Verify added shared user of manage user")
        public void verifyAddedSharedUserManageUser() throws Exception
        {
               try
               {
                      Log.testCaseInfo("TC_007 (Manage User): Verify added shared user of manage user");
                      loginPage = new LoginPage(driver).get();                
                      String uName = PropertyReader.getProperty("ManageUsername");
          			  String pWord = PropertyReader.getProperty("Password");			
          			  fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
          			  fnaHomePage.loginValidation();
                      Log.assertThat(fnaHomePage.sharedUserManageUser(), "Added Shared user of manage user Successful with valid credential", "Added Shared user of manage user unsuccessful with valid credential", driver);
               }
               catch(Exception e)
               {
            	   AnaylizeLog.analyzeLog(driver);
                      e.getCause();
                      Log.exception(e, driver);
               }
               finally
               {
                      Log.endTestCase();
                      driver.quit();
               }
         }

        /** TC_008 (Manage User): Verify Employee user of manage user 
         * Scripted By: Sekhar
        * @throws Exception
        */
        @Test(priority = 7, enabled = true, description = "TC_008 (Manage User): Verify added Employee user of manage user")
        public void verifyAddedEmployeeManageUser() throws Exception
        {
               try
               {
                      Log.testCaseInfo("TC_008 (Manage User): Verify added Employee user of manage user");
                      loginPage = new LoginPage(driver).get();                
                      String uName = PropertyReader.getProperty("Employeeusername");
          			  String pWord = PropertyReader.getProperty("Password");			
          			  fnaHomePage=loginPage.loginWithEmployeeUserCredential(uName,pWord);  
          			  fnaHomePage.loginValidation();
          			  String uName1 = PropertyReader.getProperty("ManageUsername");
         			  String pWord1 = PropertyReader.getProperty("Password");          			  
                      Log.assertThat(fnaHomePage.employeeManageUser(uName1,pWord1), "Added Employee user of manage user Successful with valid credential", "Added Employee user of manage user unsuccessful with valid credential", driver);
               }
               catch(Exception e)
               {
            	   AnaylizeLog.analyzeLog(driver);
                      e.getCause();
                      Log.exception(e, driver);
               }
               finally
               {
                      Log.endTestCase();
                      driver.quit();
               }
        }

        /** TC_009 (Manage User): Verify Employee user count of manage user 
         *  Scripted By: Sekhar
         * @throws Exception
        */
        @Test(priority = 8, enabled = true, description = "TC_009 (Manage User): Verify added Employee user count of manage user")
        public void verifyAddedEmployeecountManageUser() throws Exception
        {
               try
               {
                      Log.testCaseInfo("TC_009 (Manage User): Verify added Employee user count of manage user");
                      loginPage = new LoginPage(driver).get();   
                      String uName = PropertyReader.getProperty("Employeeusername");
          			  String pWord = PropertyReader.getProperty("Password");	
                      fnaHomePage=loginPage.loginWithEmployeeUserCredential(uName,pWord);  
                      fnaHomePage.loginValidation();
                      String uName1 = PropertyReader.getProperty("ManageUsername");
          			  String pWord1 = PropertyReader.getProperty("Password");
                      Log.assertThat(fnaHomePage.avaliablecount_Managauser(uName1,pWord1), "Added Employee user count of manage user Successful with valid credential", "Added Employee user count of manage user unsuccessful with valid credential", driver);
               }
               catch(Exception e)
               {
            	   AnaylizeLog.analyzeLog(driver);
                      e.getCause();
                      Log.exception(e, driver);
               }
               finally
               {
                      Log.endTestCase();
                      driver.quit();
               }
        }
        
        /** TC_010 (Manage User): Verify adding Employee user and shared user of manage user in different a/c. 
         * 
         * @throws Exception
        */
        @Test(priority = 9, enabled = true, description = "TC_010 (Manage User): Verify adding Employee user and shared user of manage user in different a/c")
        public void verifyAddedEmployee_shareduser_ManageUser() throws Exception
        {
               try
               {
                      Log.testCaseInfo("TC_010 (Manage User): Verify adding Employee user and shared user of manage user in different a/c.");
                      loginPage = new LoginPage(driver).get();                
                      String uName = PropertyReader.getProperty("ManageUsername");
          			  String pWord = PropertyReader.getProperty("Password");			
          			  fnaHomePage=loginPage.loginWithValidCredential(uName,pWord); 
          			  fnaHomePage.loginValidation();
                      Log.assertThat(fnaHomePage.Create_employee_sharedcontact_ManageUser(), "added Employee user and shared user of manage user in Owner a/c Successful with valid credential", "added Employee user and shared user of manage user in Owner a/c unsuccessful with valid credential", driver);
                      Log.assertThat(fnaHomePage.Create_contactuser_Addressbook(), "added contact user of address book in Owner a/c Successful with valid credential", "added contact user of address book in Owner a/c unsuccessful with valid credential", driver);
                      fnaHomePage.logout();
                      String uName1 = PropertyReader.getProperty("Employeeusername");
                      String pWord1 = PropertyReader.getProperty("Password");	
                      fnaHomePage=loginPage.loginWithEmployeeUserCredential(uName1,pWord1);                           
                      fnaHomePage.loginValidation();
                      Log.assertThat(fnaHomePage.Create_employee_sharedcontact_ManageUser(), "added Employee user and shared user of manage user in employee a/c Successful with valid credential", "added Employee user and shared user of manage user in employee a/c unsuccessful with valid credential", driver);
                      fnaHomePage.logout();
                      fnaHomePage.loginWithSecondAdminUser();                           
                      Log.assertThat(fnaHomePage.Create_employee_sharedcontact_ManageUser(), "added Employee user and shared user of manage user in Second admin a/c Successful with valid credential", "added Employee user and shared user of manage user in Second admin a/c unsuccessful with valid credential", driver);
                      Log.assertThat(fnaHomePage.Create_contactuser_Addressbook(), "added contact user of manage user in Second admin a/c Successful with valid credential", "added contact user of manage user in Second admin a/c unsuccessful with valid credential", driver);
                      fnaHomePage.logout();
               }
               catch(Exception e)
               {
            	   AnaylizeLog.analyzeLog(driver);
                      e.getCause();
                      Log.exception(e, driver);
               }
               finally
               {
                      Log.endTestCase();
                      driver.quit();
               }
        }
        
        /** TC_011 (Manage User): Verify shared user count of manage user in shared user a/c. 
         * 
         * @throws Exception
        */
        @Test(priority = 10, enabled = true, description = "TC_011 (Manage User): Verify shared user count of manage user in shared user a/c.")
        public void verifyAddedSharedUser_Sharedusercount_ManageUser() throws Exception
        {
               try
               {
                      Log.testCaseInfo("TC_011 (Manage User): Verify shared user count of manage user in shared user a/c.");
                      loginPage = new LoginPage(driver).get();                
                      fnaHomePage=loginPage.loginWithSharedUserCredential();  
                      fnaHomePage.loginValidation(); 
                      Log.assertThat(fnaHomePage.sharedUserCountManageUser(), "Added Shared user count of manage user Successful with valid credential", "Added Shared user count of manage user unsuccessful with valid credential", driver);
               }
               catch(Exception e)
               {
            	   AnaylizeLog.analyzeLog(driver);
                      e.getCause();
                      Log.exception(e, driver);
               }
               finally
               {
                      Log.endTestCase();
                      driver.quit();
               }
        }
        
        /** TC_012 (Manage User): Verify update user details of manage user and reflect other user. 
         * 
         * @throws Exception
        */
        @Test(priority = 11, enabled = true, description = "TC_012 (Manage User): Verify update user details of manage user and reflect other user")
        public void verifyEditUser_reflectOtherUser_ManageUser() throws Exception
        {
               try
               {
                      Log.testCaseInfo("TC_012 (Manage User): Verify update user details of manage user and reflect other user.");
                      loginPage = new LoginPage(driver).get();                
                      String uName = PropertyReader.getProperty("ManageUsername");
          			  String pWord = PropertyReader.getProperty("Password");			
          			  fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);  
                      fnaHomePage.loginValidation(); 
                      Log.assertThat(fnaHomePage.Create_employee_EditUser_ManageUser(), "Update user details of manage user in owner a/c Successful with valid credential", "Update user details of manage user in owner a/c unsuccessful with valid credential", driver);
                      fnaHomePage.logout();
                      fnaHomePage.loginWithSecondAdminUser();  
                      String uName1 = PropertyReader.getProperty("ManageUsername");
          			  String pWord1 = PropertyReader.getProperty("Password");
                      Log.assertThat(fnaHomePage.Create_employee_EditUser_SecondUser_ManageUser(uName1,pWord1), "Update user details of manage user in second admin a/c Successful with valid credential", "Update user details of manage user in second admin a/c unsuccessful with valid credential", driver);
               }
               catch(Exception e)
               {
            	   AnaylizeLog.analyzeLog(driver);
                      e.getCause();
                      Log.exception(e, driver);
               }
               finally
               {
                      Log.endTestCase();
                      driver.quit();
               }             
        }
        
        
        /** TC_013 (Manage User): Verify release user license of manage user. 
         * 
         * @throws Exception
        */
        @Test(priority = 12, enabled = true, description = "TC_013 (Manage User): Verify release user license of manage user.")
        public void verifyreleaseuser_license_ManageUser() throws Exception
        {
               try
               {
                      Log.testCaseInfo("TC_013 (Manage User): Verify release user license of manage user.");
                      loginPage = new LoginPage(driver).get();                
                      String uName = PropertyReader.getProperty("ManageUsername");
                      String pWord = PropertyReader.getProperty("Password");			
          			  fnaHomePage=loginPage.loginWithValidCredential(uName,pWord); 
                      fnaHomePage.loginValidation(); 
                      Log.assertThat(fnaHomePage.Create_employee_releaseuserlicense_ManageUser(), "Release user license of manage user in Owner user a/c Successful with valid credential", "Release user license of manage user in Owner user a/c unsuccessful with valid credential", driver);
                      fnaHomePage.logout();
                      fnaHomePage.loginWithSecondAdminUser();                    
                      Log.assertThat(fnaHomePage.Create_employee_releaseuserlicense_ManageUser(), "Release user license of manage user in second admin a/c Successful with valid credential", "Release user license of manage user in second admin a/c unsuccessful with valid credential", driver);
                      fnaHomePage.logout();                 
                      fnaHomePage=loginPage.loginWithSharedUserCredential();  
                      fnaHomePage.loginValidation();
                      Log.assertThat(fnaHomePage.Create_Shareduser_releaseuserlicense_ManageUser(), "Release user license of manage user in shared user a/c Successful with valid credential", "Release user license of manage user in shared user a/c unsuccessful with valid credential", driver);
                      
               }
               catch(Exception e)
               {
            	   AnaylizeLog.analyzeLog(driver);
                      e.getCause();
                      Log.exception(e, driver);
               }
               finally
               {
                      Log.endTestCase();
                      driver.quit();
               }             
        }



}
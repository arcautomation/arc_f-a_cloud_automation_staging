package com.arc.fna.testscripts;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.CommunicationPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.pages.ModuleSearchPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;


@Listeners(EmailReport.class)

public class FNA_ModuleSearch
{
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	CommunicationPage communicationPage;
	ModuleSearchPage moduleSearchPage;
	
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) 
	{		
		if(browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		

			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}
	
	/** TC_001 (Module Search): Verify user Module search in communications level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (Module Search): Verify user Module search in communications level.")
	public void verifyModulesearch_Communications() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_001 (Module Search): Verify user Module search in communications level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection24");			
			fnaHomePage.selectcollection(Collection_Name);	
			driver.switchTo().defaultContent();
			communicationPage = new CommunicationPage(driver).get();
			String SubjectName= communicationPage.Random_Subject();
			String ContactName = PropertyReader.getProperty("FNATeamFilename");	
			String Email = PropertyReader.getProperty("Coll_Favouriteuser");	
			Log.assertThat(communicationPage.Create_communication(ContactName,Email,SubjectName), "Create Communication Successful with valid credential", "Create Communication unsuccessful with valid credential", driver);
			driver.switchTo().defaultContent();
			moduleSearchPage = new ModuleSearchPage(driver).get();						
			Log.assertThat(moduleSearchPage.Module_Search_CommunicationsLevel(SubjectName), "Module search in Communications level Successfull", "Module search in Communications level UnSuccessfull", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_002 (Module Search): Verify user Edit file and module search in file level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_002 (Module Search): Verify user Edit file and module search in file level.")
	public void verifyEditFile_ModuleSearch_Files() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_002 (Module Search): Verify user Edit file and module search in file level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection24");			
			fnaHomePage.selectcollection(Collection_Name);
			String FolderName = PropertyReader.getProperty("SelFolder");			
			fnaHomePage.Select_Folder(FolderName);	
			driver.switchTo().defaultContent();
			moduleSearchPage = new ModuleSearchPage(driver).get();	
			String ReName= moduleSearchPage.Random_ReName();
			String Title= moduleSearchPage.Random_Title();
			Log.assertThat(moduleSearchPage.Edit_File_CollectionFolder(ReName,Title), "Edit File in file level Successfull", "Edit File in file level UnSuccessfull", driver);
			Log.assertThat(moduleSearchPage.Module_Search_FileLevel(ReName), "File module search in file level Successfull", "File module search in file level UnSuccessfull", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_003 (Module Search): Verify user Module search in Photos level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_003 (Module Search): Verify user Module search in Photos level.")
	public void verifyModulesearch_Photos() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_003 (Module Search): Verify user Module search in Photos level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection24");			
			fnaHomePage.selectcollection(Collection_Name);	
			driver.switchTo().defaultContent();
			moduleSearchPage = new ModuleSearchPage(driver).get();
			String Title_Name= moduleSearchPage.Random_Title();
			String Description= moduleSearchPage.Random_Description();
			Log.assertThat(moduleSearchPage.selectCollectionAlbum(Collection_Name,Title_Name,Description), "Selected Collection Album and Edit Album Successfull", "Selected Collection Album and Edit Album UnSuccessfull", driver);
			Log.assertThat(moduleSearchPage.Module_Search_Photo(Title_Name,Description), "Module search in photos Successfull", "Module search in photos UnSuccessfull", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_004 (Module Search): Verify user Grid search in File level.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 3, enabled = true, description = "TC_004 (Module Search): Verify user Grid search in File level.")
	public void verifyGrid_Search_Filelevel() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_004 (Module Search): Verify user Grid search in File level.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String Collection_Name = PropertyReader.getProperty("FNASelectCollection24");			
			fnaHomePage.selectcollection(Collection_Name);
			String FolderName = PropertyReader.getProperty("FNASelectFolder12");			
			fnaHomePage.Select_Folder(FolderName);	
			driver.switchTo().defaultContent();
			moduleSearchPage = new ModuleSearchPage(driver).get();	
			String FileName = PropertyReader.getProperty("Filename111");			
			Log.assertThat(moduleSearchPage.Grid_Search_FileLevel(FileName), "Grid search in file level Successfull", "Grid search in file level UnSuccessfull", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}

	
}
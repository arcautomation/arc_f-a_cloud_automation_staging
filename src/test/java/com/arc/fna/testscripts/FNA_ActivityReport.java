package com.arc.fna.testscripts;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.ActivityReportPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.GridExportPage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;




@Listeners(EmailReport.class)

public class FNA_ActivityReport
{
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;	
	ActivityReportPage activityReportPage;
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) 
	{
		
		if(browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		

			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}
	
	/** TC_001 (Activity Report): Verify file activity report as excel.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (Activity Report): Verify file activity report as excel.")
	public void verifyFile_Activity_Report() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_001 (Activity Report): Verify file activity report as excel.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			driver.switchTo().defaultContent();
			activityReportPage = new ActivityReportPage(driver).get();
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";			
			String DownloadPath1="C:\\" + "Users\\" + usernamedir + "\\Downloads\\";
			Log.assertThat(activityReportPage.File_ActivityReport(DownloadPath,DownloadPath1), "File activity report as excel Successfull", "File activity report as excel UnSuccessful", driver);
				
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_002 (Activity Report): Verify user activity report as excel.
	 *  Scripted By: Sekhar
	 * @throws Exception
	 */
	@Test(priority = 1, enabled = true, description = "TC_002 (Activity Report): Verify user activity report as excel.")
	public void verifyUser_Activity_Report() throws Exception
	{
			
		try
		{		
			Log.testCaseInfo("TC_002 (Activity Report): Verify user activity report as excel.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Collectionlistpro");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			driver.switchTo().defaultContent();
			activityReportPage = new ActivityReportPage(driver).get();
			String usernamedir=System.getProperty("user.name");
			String DownloadPath="C:\\" + "Users\\" + usernamedir + "\\Downloads";			
			String DownloadPath1="C:\\" + "Users\\" + usernamedir + "\\Downloads\\";
			Log.assertThat(activityReportPage.User_ActivityReport(DownloadPath,DownloadPath1), "User activity report as excel Successfull", "User activity report as excel UnSuccessful", driver);
				
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
}
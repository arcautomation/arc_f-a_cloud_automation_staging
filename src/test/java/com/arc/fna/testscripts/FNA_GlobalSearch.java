package com.arc.fna.testscripts;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.FnARegistrationPage;
import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.pages.GlobalSearchPage;
import com.arc.fna.pages.LoginPage;
import com.arc.fna.utils.AnaylizeLog;
import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;



@Listeners(EmailReport.class)

public class FNA_GlobalSearch 
{
	static WebDriver driver;
	LoginPage loginPage;
	FnaHomePage fnaHomePage;
	GlobalSearchPage globalSearchPage;
	
	
	@Parameters("browser")
	@BeforeMethod(groups="sekhar_test")
	public WebDriver beforeTest(String browser) 
	{
		
		if(browser.equalsIgnoreCase("firefox")) 
		{
			File dest = new File("./drivers/win/geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{ 
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
		
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
						
		
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("safebrowsing.enabled", "true"); 
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", chromePrefs);		
	
			options.addArguments("--start-maximized");
			driver = new ChromeDriver( options );
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));	 
		} 
		else if (browser.equalsIgnoreCase("safari"))
		{
			System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
	  return driver;
	}
	
	/** TC_001 (GlobalSearch): Verify user Global search with collection.
	 *  Scripted By: Sekhar 		 
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_001 (GlobalSearch): Verify user Global search with collection.")
	public void verifyCollection_GlobalSearch() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_001 (GlobalSearch): Verify user Global search with collection.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();	
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);
			driver.switchTo().defaultContent();
			globalSearchPage = new GlobalSearchPage(driver).get();			
			Log.assertThat(globalSearchPage.Collection_GlobalSearch(CollectionName), "Global Search with collection successfull with valid credential", "Global Search with collection unsuccessfull with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}	
	
	/** TC_002 (GlobalSearch): Verify user Global search with collection after collection info.
	 *  Scripted By: Sekhar 		 
	 * @throws Exception
	 */
	
	@Test(priority = 1, enabled = true, description = "TC_002 (GlobalSearch): Verify user Global search with collection after collection info.")
	public void verifyGlobalSearch_Collection_Info() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_002 (GlobalSearch): Verify user Global search with collection after collection info.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			driver.switchTo().defaultContent();
			globalSearchPage = new GlobalSearchPage(driver).get();
			String CollectionName= globalSearchPage.Random_Collectionname();
			String Descrip = PropertyReader.getProperty("CollectionDesc");
			String Address = PropertyReader.getProperty("CollectionAddress");
			String City = PropertyReader.getProperty("Collectioncity");
			String Zip = PropertyReader.getProperty("CollectionZip");
			String Country = PropertyReader.getProperty("CollectionCountry");
			String State = PropertyReader.getProperty("CollectionState");
			globalSearchPage.Create_Collections_Search(CollectionName,Descrip,Address,City,Zip,Country,State);	
			Log.assertThat(globalSearchPage.Collection_GlobalSearch(CollectionName), "Global Search with collection successfull with valid credential", "Global Search with collection unsuccessfull with valid credential", driver);
			Log.assertThat(globalSearchPage.GlobalSearch_Collection_Info(CollectionName,Address,City,Zip,Country,State), "Global Search with collection info successfull with valid credential", "Global Search with collection info unsuccessfull with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_003 (GlobalSearch): Verify user Global search with file after four buttons display or not.
	 *  Scripted By: Sekhar 		 
	 * @throws Exception
	 */
	@Test(priority = 2, enabled = true, description = "TC_003 (GlobalSearch): Verify user Global search with file after four buttons display or not.")
	public void verifyFile_GlobalSearch_FourButtons() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_003 (GlobalSearch): Verify user Global search with file after four buttons display or not.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			String CollectionName= fnaHomePage.Random_Collectionname();					
			fnaHomePage.Create_Collections(CollectionName);			
			String FolderName= fnaHomePage.Random_Foldername();		
			Log.assertThat(fnaHomePage.Adding_Folder(FolderName), "Adding folder Successfull", "Adding folder UnSuccessful", driver);
			File Path=new File(PropertyReader.getProperty("GlobalFilePath"));			
			String FolderPath = Path.getAbsolutePath().toString();			
			Log.assertThat(fnaHomePage.UploadFile(FolderPath), "Upload file Successfull", "Upload file UnSuccessful", driver);
			driver.switchTo().defaultContent();
			globalSearchPage = new GlobalSearchPage(driver).get();			
			Log.assertThat(globalSearchPage.File_GlobalSearch_Fourbuttons(CollectionName), "Global Search with file name after 4 buttons displayed", "Global Search with file name after 4 buttons not displayed ", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}

	/** TC_004 (GlobalSearch): Verify user Edit collection after Global search with collection name.
	 *  Scripted By: Sekhar 		 
	 * @throws Exception
	 */
	
	@Test(priority = 3, enabled = true, description = "TC_004 (GlobalSearch): Verify user Edit collection after Global search with collection name.")
	public void verifyEdit_Collection_GlobalSearch() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_004 (GlobalSearch): Verify user Edit collection after Global search with collection name.");
			loginPage = new LoginPage(driver).get();		
			String uName = PropertyReader.getProperty("Coll_Favouriteuser");
			String pWord = PropertyReader.getProperty("Password");			
			fnaHomePage=loginPage.loginWithValidCredential(uName,pWord);	
			fnaHomePage.loginValidation();
			driver.switchTo().defaultContent();
			globalSearchPage = new GlobalSearchPage(driver).get();
			String CollectionName= globalSearchPage.Random_Collectionname();
			String Descrip = PropertyReader.getProperty("CollectionDesc");
			String Address = PropertyReader.getProperty("CollectionAddress");
			String City = PropertyReader.getProperty("Collectioncity");
			String Zip = PropertyReader.getProperty("CollectionZip");
			String Country = PropertyReader.getProperty("CollectionCountry");
			String State = PropertyReader.getProperty("CollectionState");
			globalSearchPage.Create_Collections_Search(CollectionName,Descrip,Address,City,Zip,Country,State);	
			String Edit_CollectionName= globalSearchPage.Random_EditCollectionname();
			Log.assertThat(globalSearchPage.Edit_Collection(CollectionName,Edit_CollectionName), "Edit collection successfull with valid credential", "Edit collection unsuccessfull with valid credential", driver);
			Log.assertThat(globalSearchPage.Collection_GlobalSearch(Edit_CollectionName), "Global Search with Edit collection successfull with valid credential", "Global Search with Edit collection unsuccessfull with valid credential", driver);
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_005 (GlobalSearch): Verify New Registration user default Custom Attributes showing or not under global search.
	 *  Scripted By: Sekhar 
	 * @throws Exception
	 */
	@Test(priority = 4, enabled = false, description = "TC_005 (GlobalSearch): Verify New Registration user default Custom Attributes showing or not under global search.")
	public void verifyNewRegistration_CustomAttributes_Globalsearch() throws Exception
	{			
		try
		{		
			Log.testCaseInfo("TC_005 (GlobalSearch): Verify New Registration user default Custom Attributes showing or not under global search.");
			globalSearchPage = new GlobalSearchPage(driver).get();
			String FristName= globalSearchPage.Random_TestName();			
			String State = PropertyReader.getProperty("CollectionState11");
			String Phonenum = PropertyReader.getProperty("CollectionPhonenum111");	
			String password = PropertyReader.getProperty("Password");	
			Log.assertThat(globalSearchPage.NewRegistration_CustomAttributes_GlobalSearch(FristName,State,Phonenum,password),"Default Custom Attributes in Global search successfull", "Default Custom Attributes in Global search Unsuccessfull", driver);	
		}
		catch(Exception e)
		{
			AnaylizeLog.analyzeLog(driver);
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		} 
	}
	
	
}
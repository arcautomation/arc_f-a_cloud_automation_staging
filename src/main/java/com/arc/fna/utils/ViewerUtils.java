package com.arc.fna.utils;

import com.arcautoframe.utils.Log;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

public class ViewerUtils {

    public static boolean compareScreenShots(String screenShot1, String screenShot2) {
        try {
            BufferedImage image1 = ImageIO.read(new File(screenShot1));
            BufferedImage image2 = ImageIO.read(new File(screenShot2));
            Log.message("Ensuring the equality of the widths and heights of both the screenshots.");
            if ((image1.getWidth() == image2.getWidth()) && (image1.getHeight() == image2.getHeight())) {
                int width = image1.getWidth();
                int height = image1.getHeight();
                for (int x = 0; x < width; x++) {
                    for (int y = 0; y < height; y++) {
                        if (image1.getRGB(x, y) != image2.getRGB(x, y)) {
                            Log.message("Provided screenshots are different.");
                            throw new Exception();
                        }
                    }
                }
                return true;
            }
            else {
                Log.message("The widths and heights of both the screenshots are not equal.");
                throw new Exception();
            }

        } catch (Exception e) {
            return false;
        }
    }

}

package com.arc.fna.utils;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;


public final class randomFileName 
{

	private SecureRandom random = new SecureRandom();
	
	public String nextFileName() 
	{
		    return new BigInteger(130, random).toString(32);
	}	
	
	public static String RandamName() throws java.text.ParseException {
		String name = null;

		name = "IndiaArc" + getRandomNumberInRange(00000, 99999);
		System.out.println(name);
		return name;
	}
	
	public static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
	
}


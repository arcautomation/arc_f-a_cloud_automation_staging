package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class ManagerolePage extends LoadableComponent<ManagerolePage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load()
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error
	{
		if (!isPageLoaded) 
		{
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public ManagerolePage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	
	/**
	 * Method written for Random Name(Employee) 
	 * Scripted By: Sekhar
	 * @return
	 */

	public String Random_Projectrole() 
	{

		String str = "Projectrole";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}
	
	 @FindBy(xpath = "//*[@id='liManageRole']/a")
	 WebElement btnmanagerole;
	       
	 @FindBy(xpath = "//*[@id='btnnewProjectrole']")
	 WebElement btnnewProjectrole;
	 
	 @FindBy(xpath = ".//*[@id='txtProjectRole']")
	 WebElement txtProjectRole;
	 
	 @FindBy(xpath = "//input[@class='btn btn-primary']")
	 WebElement btnsave;
	       
	 /** 
	  * Method written for Add manage project role 
	  * Scripted By: Sekhar      
	  */
	   	
	 public boolean Add_Manage_Projectrole(String Projectrole)
	 {     
		 boolean result1=false;
		 SkySiteUtils.waitTill(5000);
		 btnSetting.click();
		 Log.message("Setting button has been clicked");
		 SkySiteUtils.waitTill(3000);
		 btnmanagerole.click();
		 Log.message("Manage role button has been clicked");
		 SkySiteUtils.waitTill(5000);
		 driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame
		 SkySiteUtils.waitTill(2000);
		 btnnewProjectrole.click();
		 Log.message("Add button has been clicked");
		 SkySiteUtils.waitTill(5000);
		 txtProjectRole.sendKeys(Projectrole);
		 Log.message("Project role has been entered");
		 SkySiteUtils.waitTill(3000);
		 btnsave.click();
		 Log.message("save button has been clicked");
		 SkySiteUtils.waitTill(3000);
		 
		 int prjroleCount = driver.findElements(By.xpath("//*[@id='DivCustomRoleGrid']/div[2]/table/tbody/tr/td[1]")).size();
		 Log.message("project role Count is:" + prjroleCount);
		
		 int i = 0;
		 for(i=1; i <= prjroleCount; i++) 
		 {			
			 String prjroleName = driver.findElement(By.xpath("(//*[@id='DivCustomRoleGrid']/div[2]/table/tbody/tr/td[1])["+ i+"]")).getText();
			 Log.message(prjroleName);
			 SkySiteUtils.waitTill(2000);			 
			 if (prjroleName.contains(Projectrole)) 
			 {
				 Log.message("Exp project role Successfully!!!!");
				 result1=true;
				 break;
			 }	
		 }
		 SkySiteUtils.waitTill(5000);   		
		 if(result1==true) 
			 return true;
		 else
			 return false;	   		
	 }
	
}
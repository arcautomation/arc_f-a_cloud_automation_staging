package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class MessageboardPage extends LoadableComponent<MessageboardPage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	FnaHomePage fnaHomePage;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load()
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public MessageboardPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}	
	
	/**
	 * Method written for Random Name
	 *  Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_FileName()
	{
		String str = "File";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;
	}
	
	@FindBy(css = "#Button1")
	WebElement btnmore;
	
	@FindBy(css = "#liSubscribeAlert1>a")
	WebElement btnliSubscribeAlert1;
	
	@FindBy(css = "#chkFolderFileAdd")
	WebElement btnchkFolderFileAdd;
	
	@FindBy(css = "#btnSaveAlertSettings")
	WebElement btnSaveAlertSettings;
	
	@FindBy(xpath = "//span[@class='noty_text']")
	WebElement notifymsg;	
	
	@FindBy(css = "#btnUploadFile")
	WebElement btnUploadFile;
	
	//@FindBy(xpath = "//*[@id='fine-uploader']/div/div[2]/div/div[2]/input")
	@FindBy(css = "#btnSelectFiles")//modify sekhar
	WebElement btnselctFile;

	//@FindBy(xpath = "//button[@id='btnFileUpload']")
	@FindBy(xpath = ".//*[@id='multipartUploadBtn']")
	WebElement btnFileUpload;
	
	@FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")
	WebElement btnFile;
		
	@FindBy(xpath = "//*[@id='ProjectMenu1_Communications']/div/a[2]")
	WebElement btnCommunicationsdrpdwn;	
	
	@FindBy(css = "#ProjectMenu1_liMessageBoard>a")
	WebElement btnProjectMenuliMessageBoard;
	
	@FindBy(xpath = ".//*[@id='divGrid1']/div[2]/table/tbody/tr[2]/td[2]")
	WebElement pathSubject;	
	
	/** 
     * Method written for Add file after the set alert.
     * Scripted By: Sekhar     
	 * @throws IOException 
     * @throws AWTException 
     */
	
    public boolean Addfile_After_Setalert_Verify_Messageboard(String FilePath) throws IOException
    {    
    	boolean result1=false;
    	boolean result2=false;
    	
    	SkySiteUtils.waitTill(3000);    	
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	Log.message("Switch to frame.......");
    	btnmore.click();
    	Log.message("more button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnliSubscribeAlert1.click();
    	Log.message("Set alert button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnchkFolderFileAdd.click();
    	Log.message("Add file to the folder check box button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnSaveAlertSettings.click();
    	Log.message("Set alerts button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().defaultContent();
    	SkySiteUtils.waitForElement(driver, notifymsg, 60);
		String Notify = notifymsg.getText();
		//Alert settings successfully saved.
    	if(Notify.equals("Alert settings successfully saved."))
    	{
    		result1=true;
    		Log.message("Set alert successfully saved");
    	}
    	else
    	{
    		result1=false;
    		Log.message("Set alert Unsuccessfully saved");
    	}
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	btnUploadFile.click(); // Adding on New Folder
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(20000);
		btnselctFile.click();
		Log.message("select files has been clicked");
		SkySiteUtils.waitTill(5000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		//String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));

		String expFilename = null;
		File[] files = new File(FilePath).listFiles();

		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();

		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + FilePath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);

		try {
			File file = new File(tmpFileName);
			if (file.delete()) 
			{
				Log.message(file.getName() + " is deleted!");
			}
			else
			{
				Log.message("Delete operation is failed.");
			}
		}
		catch (Exception e)
		{
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(10000);
		btnFileUpload.click();// Clicking on Upload button
		Log.message("Upload button has been clicked");
		SkySiteUtils.waitTill(30000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		if(btnFile.isDisplayed())
		{
			result2=true;
			Log.message("file uploaded successfully");
		}
		else
		{
			result2=false;
			Log.message("file uploaded Unsuccessfully");
		}
		SkySiteUtils.waitTill(100000);		
    	driver.switchTo().defaultContent();    			
    	btnCommunicationsdrpdwn.click();
    	Log.message("Communication drop down button has been clicked");
    	SkySiteUtils.waitTill(5000);
    	btnProjectMenuliMessageBoard.click();
    	Log.message("Message board button has been clicked");
    	SkySiteUtils.waitTill(50000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	SkySiteUtils.waitTill(2000);
    	driver.findElement(By.xpath(".//*[@id='refreshtree']")).click();
    	SkySiteUtils.waitTill(2000);
    	if((pathSubject.isDisplayed())&&(result1==true)&&(result2==true))
    		return true;
    	else
    		return false;    	
    }
    
    @FindBy(xpath = "//*[@id='chkAllProjectdocs']")
	WebElement btnchkAllProjectdocs;	
    
    @FindBy(xpath = "//*[@id='chkFolderFileChange']")
   	WebElement btnchkFolderFileChange;
    
    @FindBy(css = ".icon.icon-info.icon-orange")
   	WebElement btninfoiconorange;
    
    @FindBy(css = "#txt_fileName")
   	WebElement txtfileName;
    
   // @FindBy(xpath = "//*[@id='div_otherInfo']/div/div/div[3]/input[1]")
    @FindBy(xpath = "//*[@id='btnupdateclose']")    
   	WebElement btnupdateclose;
    
    @FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")
   	WebElement filename;
    
    @FindBy(xpath = "//i[@class='icon icon-star ico-lg icon-files']")
   	WebElement btniconfiles;
    
    @FindBy(xpath = ".//*[@id='chkFolderFileDelete']")
   	WebElement btnchkFolderFileDelete;
    
    @FindBy(css = "#liDeleteFile1>a")
   	WebElement btnliDeleteFile1;
    
    @FindBy(css = "#button-1")
   	WebElement btnbutton1;
    
    
    /** 
     * Method written for Edit and Delete file after the set alert.
     * Scripted By: Sekhar     
	 * @throws IOException 
     * @throws AWTException 
     */
	
    public boolean Edit_Delete_file_After_Setalert_Verify_Messageboard(String FileName,String Collectionname) throws IOException
    {   
    	boolean result1=false;
    	boolean result2=false;
    	boolean result3=false;
    	
    	SkySiteUtils.waitTill(3000);	
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	Log.message("Switch to frame.......");
    	btnchkAllProjectdocs.click();
    	Log.message("all check box button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnmore.click();
    	Log.message("more button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnliSubscribeAlert1.click();
    	Log.message("Set alert button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnchkFolderFileChange.click();
    	Log.message("Change file in folder check box button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnSaveAlertSettings.click();
    	Log.message("Set alerts button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().defaultContent();
    	SkySiteUtils.waitForElement(driver, notifymsg, 60);
		String Notify = notifymsg.getText();
		Log.message("Notify message is:"+Notify);
		SkySiteUtils.waitTill(3000);		
    	if(Notify.equals("Alert settings successfully saved."))
    	{
    		result1=true;
    		Log.message("Set alert successfully saved");
    	}
    	else
    	{
    		result1=false;
    		Log.message("Set alert Unsuccessfully saved");
    	}
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	btninfoiconorange.click();
    	Log.message("Update button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	txtfileName.clear();
    	Log.message("file name has been cleared");
    	SkySiteUtils.waitTill(3000);
    	txtfileName.sendKeys(FileName);
    	Log.message("file name has been entered:"+FileName);
    	SkySiteUtils.waitTill(3000);
    	btnupdateclose.click();
    	Log.message("Update&close button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	String File_Name=filename.getText();
    	Log.message("File Name is:"+File_Name);
    	if(File_Name.contains(FileName))
    	{
    		result2=true;
    		Log.message("File Name updated successfully");
    	}
    	else
    	{
    		result2=false;
    		Log.message("File Name updated Unsuccessfully");
    	}
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().defaultContent();    			
    	btnCommunicationsdrpdwn.click();
    	Log.message("Communication drop down button has been clicked");
    	SkySiteUtils.waitTill(2000);
    	btnProjectMenuliMessageBoard.click();
    	Log.message("Message board button has been clicked");
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	SkySiteUtils.waitTill(2000);
    	if(pathSubject.isDisplayed())
    	{
    		result3=true;	
    		Log.message("Subject path displayed successfully");
    	}
    	else
    	{
    		result3=false;
    		Log.message("Subject path displayed Unsuccessfully");
    	}
    	SkySiteUtils.waitTill(2000);
    	driver.switchTo().defaultContent();	
    	btniconfiles.click();
    	Log.message("files button has been clicked");
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	Log.message("Switch to frame.......");
    	btnchkAllProjectdocs.click();
    	Log.message("all check box button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnmore.click();
    	Log.message("more button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnliSubscribeAlert1.click();
    	Log.message("Set alert button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnchkFolderFileDelete.click();
    	Log.message("Delete file from folder check box button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnSaveAlertSettings.click();
    	Log.message("Set alerts button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnmore.click();
    	Log.message("more button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnliDeleteFile1.click();
    	Log.message("Delete files button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().defaultContent();
    	btnbutton1.click();
    	Log.message("Yes button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnCommunicationsdrpdwn.click();
    	Log.message("Communication drop down button has been clicked");
    	SkySiteUtils.waitTill(2000);
    	btnProjectMenuliMessageBoard.click();
    	Log.message("Message board button has been clicked");
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	SkySiteUtils.waitTill(2000);
    	String Subject=pathSubject.getText();
    	Log.message("Subject is:"+Subject);
    	if((Subject.contains("File deletion Alert for Collection: "+Collectionname))
    			&&(result1==true)&&(result2==true)&&(result3==true))  	
    		return true;
    	else
    		return false;
    }
    
    @FindBy(css = "#lftpnlMore")
   	WebElement btnlftpnlMore;
    
    @FindBy(xpath = "//i[@class='icon icon-folder']")
   	WebElement btniconfolder;
    
    @FindBy(xpath = "//*[@id='UploadWithTurbo']")
   	WebElement btnUploadWithTurbo;
    
    @FindBy(xpath = "(//i[@class='icon icon-refresh ico-lg'])[1]")
   	WebElement btniconrefresh;
    
    @FindBy(xpath = "//*[@id='refreshtree']")
   	WebElement btnrefreshtree;
    
    @FindBy(xpath = "//*[@id='ViewGrid']/div/table/tbody/tr[3]/td/div/table/tbody/tr/td[3]/a")
   	WebElement btnsbjtDownload;
    
    /** 
     * Method written for Add file via trubo after set alert and download in message board.
     * Scripted By: Sekhar     
	 * @throws IOException 
     * @throws AWTException 
     */
	
    public boolean Addfile_Viaturbo_After_Setalert_Download_Messageboard(String FilePath,String Collectionname,String DownloadPath) throws AWTException, IOException
    {   
    	boolean result1=false;    	
    	SkySiteUtils.waitTill(3000); 
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	btnmore.click();
    	Log.message("more button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnliSubscribeAlert1.click();
    	Log.message("Set alert button has been clicked");
    	SkySiteUtils.waitTill(3000);    	
    	btnchkFolderFileAdd.click();
    	Log.message("Add file to the folder check box button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnSaveAlertSettings.click();
    	Log.message("Set alerts button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnlftpnlMore.click();
    	Log.message("more(left side) button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btniconfolder.click();
    	Log.message("upload folder button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().defaultContent();
    	btnUploadWithTurbo.click();
    	Log.message("upload with turbo button has been clicked");
    	SkySiteUtils.waitTill(3000);    	
    	// Handling Download PopUp using robot
    	Robot robot = null;
    	robot = new Robot();
    	SkySiteUtils.waitTill(5000);
    	robot.keyPress(KeyEvent.VK_LEFT);
    	robot.keyRelease(KeyEvent.VK_LEFT);
    	SkySiteUtils.waitTill(2000);
    	robot.keyPress(KeyEvent.VK_ENTER);
    	robot.keyRelease(KeyEvent.VK_ENTER);
    	SkySiteUtils.waitTill(50000);
    	robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		SkySiteUtils.waitTill(20000);
		robot.keyPress(KeyEvent.VK_ENTER);
    	robot.keyRelease(KeyEvent.VK_ENTER);
    	SkySiteUtils.waitTill(5000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		//String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		
		String expFilename = null;
		File[] files = new File(FilePath).listFiles();
		
		for (File file : files)
		{
			if (file.isFile())
			{
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();
		
		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + FilePath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);

		try 
		{
			File file = new File(tmpFileName);
			if (file.delete()) 
			{
				Log.message(file.getName() + " is deleted!");
			}
			else
			{
				Log.message("Delete operation is failed.");
			}
		}
		catch (Exception e)
		{
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(15000);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		SkySiteUtils.waitTill(15000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(30000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(10000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		btniconrefresh.click();
		Log.message("resfresh button has been clicked");
		SkySiteUtils.waitTill(5000);
		String FileName=driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).getText();
		Log.message("File name is:"+FileName);
		
		driver.switchTo().defaultContent();
		btnCommunicationsdrpdwn.click();
    	Log.message("Communication drop down button has been clicked");
    	SkySiteUtils.waitTill(2000);
    	btnProjectMenuliMessageBoard.click();
    	Log.message("Message board button has been clicked");
    	SkySiteUtils.waitTill(120000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	SkySiteUtils.waitTill(2000);
    	btnrefreshtree.click();
    	Log.message("refresh button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	String Subject=pathSubject.getText();
    	Log.message("Subject is:"+Subject);
    	if(Subject.contains("New File Alert for Collection:"+Collectionname))

    	{
    		result1=true;
    		Log.message("Subject path displayed successfully");
    	}
    	else
    	{
    		result1=false;
    		Log.message("Subject path displayed Unsuccessfully");
    	}
    	SkySiteUtils.waitTill(3000);
    	pathSubject.click();
    	Log.message("Subject Path has been clicked");
    	SkySiteUtils.waitTill(3000);    	
    	driver.switchTo().defaultContent();
   		fnaHomePage = new FnaHomePage(driver).get();
    	// Calling delete files from download folder script
   		fnaHomePage.Delete_Files_From_Folder(DownloadPath);
    	SkySiteUtils.waitTill(5000); 
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	btnsbjtDownload.click();
    	Log.message("Download button has been clicked");
    	SkySiteUtils.waitTill(3000); 
		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot1 = null;
			robot1 = new Robot();			
			robot1.keyPress(KeyEvent.VK_ALT);		
			robot1.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(4000);
			robot1.keyRelease(KeyEvent.VK_S);
			robot1.keyRelease(KeyEvent.VK_ALT);			
			SkySiteUtils.waitTill(3000);
			robot1.keyPress(KeyEvent.VK_ENTER);
			robot1.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
		SkySiteUtils.waitTill(25000);
		// After checking whether download file or not
		String ActualFilename = null;

		File[] files1 = new File(DownloadPath).listFiles();

		for (File file : files1) 
		{
			if (file.isFile())
			{
				ActualFilename = file.getName();// Getting File Name into a variable
				Log.message("Actual File name is:" + ActualFilename);
				SkySiteUtils.waitTill(1000);
				Long ActualFileSize = file.length();
				Log.message("Actual File size is:" + ActualFileSize);
				SkySiteUtils.waitTill(5000);
				if (ActualFileSize != 0)
				{
					Log.message("Downloaded file is available in downloads!!!");
				} 
				else
				{
					Log.message("Downloaded file is NOT available in downloads!!!");
				}
			}
		}
		if((ActualFilename.contains(FileName))&&(result1==true))
			return true;
		else
			return false;				
    }
    
    @FindBy(css = "#txtSerachValue")
   	WebElement txtSerachValue;
    
    @FindBy(css = "#btnSearch")
   	WebElement btnSearch;
    
    @FindBy(xpath = "//*[@id='divGrid1']/div[2]/table/tbody/tr[2]/td[2]")
   	WebElement subjectPath;
    
    
    /** 
     * Method written for Add file via trubo after set alert and download in message board.
     * Scripted By: Sekhar     
	 * @throws IOException 
     * @throws AWTException 
     */
	
    public boolean Search_WithSubject_Messageboard(String SearchSubject) 
    {     	
    	SkySiteUtils.waitTill(5000); 
    	btnCommunicationsdrpdwn.click();
    	Log.message("Communication drop down button has been clicked");
    	SkySiteUtils.waitTill(2000);
    	btnProjectMenuliMessageBoard.click();
    	Log.message("Message board button has been clicked");
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	SkySiteUtils.waitTill(2000);
    	txtSerachValue.sendKeys(SearchSubject);
    	Log.message("Subject name has been entered");
    	SkySiteUtils.waitTill(3000);
    	btnSearch.click();
    	Log.message("Search button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	String Subject=subjectPath.getText();
    	Log.message("Subject name is:"+Subject);
    	SkySiteUtils.waitTill(3000);
    	if(Subject.contains(SearchSubject))
    		return true;
    	else
    		return false;       	
    }
   
   // @FindBy(css = ".Body>div>p>a")
   //WebElement clickhere;
    
    @FindBy(css = ".Body>div>table>tbody>tr>td>a")
   	WebElement clickhere;
    /** 
     * Method written for Download link Add file After Set alert in Message board.
     * Scripted By: Sekhar     
	 * @throws IOException 
     * @throws AWTException 
     */
	
    public boolean Downloadlink_Addfile_After_Setalert_Messageboard(String FilePath,String DownloadPath) throws IOException, AWTException
    {    
    	boolean result1=false;
    	boolean result2=false;
    	
    	SkySiteUtils.waitTill(3000);    	
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	Log.message("Switch to frame.......");
    	btnmore.click();
    	Log.message("more button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnliSubscribeAlert1.click();
    	Log.message("Set alert button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnchkFolderFileAdd.click();
    	Log.message("Add file to the folder check box button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	btnSaveAlertSettings.click();
    	Log.message("Set alerts button has been clicked");
    	SkySiteUtils.waitTill(8000);        
    	btnUploadFile.click(); // Adding on New Folder
		Log.message("upload button has been clicked");
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(15000);
		btnselctFile.click();
		Log.message("select files has been clicked");
		SkySiteUtils.waitTill(5000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		//String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		String tmpFileName = "./AutoIT_Temp/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));

		String expFilename = null;
		File[] files = new File(FilePath).listFiles();

		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(5000);
			}
		}
		output.flush();
		output.close();

		String AutoItexe_Path = PropertyReader.getProperty("AutoItexe1");
		// Executing .exe autoIt file
		Runtime.getRuntime().exec(AutoItexe_Path + " " + FilePath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);

		try {
			File file = new File(tmpFileName);
			if (file.delete()) 
			{
				Log.message(file.getName() + " is deleted!");
			}
			else
			{
				Log.message("Delete operation is failed.");
			}
		}
		catch (Exception e)
		{
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(10000);
		btnFileUpload.click();// Clicking on Upload button
		Log.message("Upload button has been clicked");
		SkySiteUtils.waitTill(30000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		SkySiteUtils.waitTill(5000);
		if(btnFile.isDisplayed())
		{
			result1=true;
			Log.message("file uploaded successfully");
		}
		else
		{
			result1=false;
			Log.message("file uploaded Unsuccessfully");
		}
		SkySiteUtils.waitTill(3000);
		String FileName=driver.findElement(By.xpath(".//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).getText();
		Log.message("File name is:"+FileName);
		SkySiteUtils.waitTill(100000);
    	driver.switchTo().defaultContent();    			
    	btnCommunicationsdrpdwn.click();
    	Log.message("Communication drop down button has been clicked");
    	SkySiteUtils.waitTill(2000);    	
    	btnProjectMenuliMessageBoard.click();
    	Log.message("Message board button has been clicked");
    	SkySiteUtils.waitTill(20000);
    	driver.switchTo().defaultContent();
   		fnaHomePage = new FnaHomePage(driver).get();
    	// Calling delete files from download folder script
   		fnaHomePage.Delete_Files_From_Folder(DownloadPath);
    	SkySiteUtils.waitTill(5000); 
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	SkySiteUtils.waitTill(2000);
    	driver.findElement(By.xpath(".//*[@id='refreshtree']")).click();
    	SkySiteUtils.waitTill(2000);
    	pathSubject.click();
    	Log.message("Subject has been clicked");
    	SkySiteUtils.waitTill(5000);    	
    	clickhere.click();
    	Log.message("clickhere has been clicked");
    	SkySiteUtils.waitTill(3000);
    	/*String parentHandle1 = driver.getWindowHandle();
		Log.message(parentHandle1);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(5000);*/	
    	
		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);
		
		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot1 = null;
			robot1 = new Robot();			
			robot1.keyPress(KeyEvent.VK_ALT);		
			robot1.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(4000);
			robot1.keyRelease(KeyEvent.VK_S);
			robot1.keyRelease(KeyEvent.VK_ALT);			
			SkySiteUtils.waitTill(3000);
			robot1.keyPress(KeyEvent.VK_ENTER);
			robot1.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
		SkySiteUtils.waitTill(25000);
		// After checking whether download file or not
		String ActualFilename = null;
		
		File[] files1 = new File(DownloadPath).listFiles();
		
		for (File file : files1) 
		{
			if (file.isFile())
			{
				ActualFilename = file.getName();// Getting File Name into a variable
				Log.message("Actual File name is:" + ActualFilename);
				SkySiteUtils.waitTill(1000);
				Long ActualFileSize = file.length();
				Log.message("Actual File size is:" + ActualFileSize);
				SkySiteUtils.waitTill(5000);
				if (ActualFileSize != 0)
				{
					Log.message("Downloaded file is available in downloads!!!");
				} 
				else
				{
					Log.message("Downloaded file is NOT available in downloads!!!");
				}
			}
		}
		if((ActualFilename.contains(FileName))&&(result1==true))
			return true;
		else
			return false;		
    }
    
}
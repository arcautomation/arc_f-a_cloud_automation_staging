package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class ArchivedCollectionsPage extends LoadableComponent<ArchivedCollectionsPage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	FnaHomePage fnaHomePage;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public ArchivedCollectionsPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	@FindBy(css = "#Collections>a")
	WebElement btnCollections;
	
	@FindBy(css = "#btnResetSearch")
	WebElement btnResetSearch;
	
	@FindBy(css = "#tbPrjSetting>span>a")
	WebElement btntbPrjSetting;
	
	@FindBy(css = "#btnExport")
	WebElement btnExport;
	
	
	/** 
     * Method written for Verify Archive Export in collection setting
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Verify_Archive_Export(String CollectionName)
    {
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().defaultContent();
    	btnCollections.click();
    	Log.message("collections button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame   		
    	btnResetSearch.click();
    	Log.message("Reset button has been clicked");
    	SkySiteUtils.waitTill(2000);
    	int collec_chechboxCount = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
   		Log.message("collection chechbox Count is:" + collec_chechboxCount);
   		// Loop start '2' as collection list start from tr[2]
   		int i = 0;
   		for (i = 1; i <= collec_chechboxCount; i++)
   		{
   			// x path as dynamically providing all rows(i) tr[i]
   			String collName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+i+"]")).getText();
   			Log.message(collName);
   			SkySiteUtils.waitTill(2000);   		
   			// Checking project name equal with each row of table
   			if (collName.equals(CollectionName)) 
   			{
   				Log.message("Select Collection edit button has been clicked!!!");
   				break;
   			}
   		}
   		SkySiteUtils.waitTill(3000);
   		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a)["+ i+"]")).click();
   		Log.message("Edit button has been clicked");
   		SkySiteUtils.waitTill(5000);
   		String parentHandle = driver.getWindowHandle(); //Getting parent window
  		Log.message(parentHandle);
  		for(String winHandle : driver.getWindowHandles())
  		{
  			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
  		}
  		SkySiteUtils.waitTill(5000);
  		driver.switchTo().defaultContent();
  		btntbPrjSetting.click();
  		Log.message("Setting button has been clicked");
  		SkySiteUtils.waitTill(3000);
  		SkySiteUtils.waitForElement(driver, btnExport, 120);
  		if(btnExport.isDisplayed())
  			return true;
  		else
  			return false;  	
    }
    
    
    @FindBy(xpath = "//input[@name='btnArchive' and @disabled='']")
	WebElement btndisabled;
    
    @FindBy(xpath = "//input[@id='btnArchive']")
	WebElement btnArchive;
    
    @FindBy(xpath = "//*[@id='txtArchiveName']")
   	WebElement txtArchiveName;
	
	
	/** 
     * Method written for Verify Active Archive button in collection setting
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Verify_Active_Archivebutton(String CollectionName)
    {
    	boolean result1=true;
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().defaultContent();
    	btnCollections.click();
    	Log.message("collections button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame   		
    	btnResetSearch.click();
    	Log.message("Reset button has been clicked");
    	SkySiteUtils.waitTill(2000);
    	int collec_chechboxCount = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
   		Log.message("collection chechbox Count is:" + collec_chechboxCount);
   		// Loop start '2' as collection list start from tr[2]
   		int i = 0;
   		for (i = 1; i <= collec_chechboxCount; i++)
   		{
   			// x path as dynamically providing all rows(i) tr[i]
   			String collName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+i+"]")).getText();
   			Log.message(collName);
   			SkySiteUtils.waitTill(2000);   		
   			// Checking project name equal with each row of table
   			if (collName.equals(CollectionName)) 
   			{
   				Log.message("Select Collection edit button has been clicked!!!");
   				break;
   			}
   		}
   		SkySiteUtils.waitTill(3000);
   		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a)["+ i+"]")).click();
   		Log.message("Edit button has been clicked");
   		SkySiteUtils.waitTill(3000);
   		String parentHandle = driver.getWindowHandle(); //Getting parent window
  		Log.message(parentHandle);
  		for(String winHandle : driver.getWindowHandles())
  		{
  			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
  		}
  		SkySiteUtils.waitTill(3000);
  		driver.switchTo().defaultContent();
  		btntbPrjSetting.click();
  		Log.message("Setting button has been clicked");
  		SkySiteUtils.waitTill(5000); 	
  		Select drpcountry = new Select(driver.findElement(By.name("ddlProjectStatus")));
		drpcountry.selectByVisibleText("Complete");
		Log.message("Complete has been selected");
		SkySiteUtils.waitTill(5000);
		btnArchive.click();
		Log.message("Archive button has been clicked");
		SkySiteUtils.waitTill(3000);
		if(txtArchiveName.isDisplayed())
			return true;
		else
			return false;		
    }
    
    @FindBy(xpath = "//*[@id='_cheCollectionInformation']")
   	WebElement cheCollectionInformation;	
    
    @FindBy(xpath = "//*[@id='btnSave']")
   	WebElement btnSave;
    
    @FindBy(xpath = "//*[@id='btnMsgAlertClose']")
   	WebElement btnMsgAlertClose;
    
    @FindBy(css = "#titleStat")
   	WebElement btntitleStat;
    
    @FindBy(css = "#ancArchivedProjects")
   	WebElement btnancArchivedProjects;
    
   
    @FindBy(xpath = "//*[@id='btnClose']")
   	WebElement btnClose;
    
    
    /** 
     * Method written for Verify Edit  in Archived collections
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Verify_Edit_Archivedcollections(String CollectionName)
    {
    	boolean result1=true;
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().defaultContent();
    	btnCollections.click();
    	Log.message("collections button has been clicked");
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame   		
    	btnResetSearch.click();
    	Log.message("Reset button has been clicked");
    	SkySiteUtils.waitTill(2000);
    	int collec_chechboxCount = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
   		Log.message("collection chechbox Count is:" + collec_chechboxCount);
   		// Loop start '2' as collection list start from tr[2]
   		int i = 0;
   		for (i = 1; i <= collec_chechboxCount; i++)
   		{
   			// x path as dynamically providing all rows(i) tr[i]
   			String collName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+i+"]")).getText();
   			Log.message(collName);
   			SkySiteUtils.waitTill(2000);   		
   			// Checking project name equal with each row of table
   			if (collName.equals(CollectionName)) 
   			{
   				Log.message("Select Collection edit button has been clicked!!!");
   				break;
   			}
   		}
   		SkySiteUtils.waitTill(3000);
   		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a)["+ i+"]")).click();
   		Log.message("Edit button has been clicked");
   		SkySiteUtils.waitTill(3000);
   		String parentHandle = driver.getWindowHandle(); //Getting parent window
  		Log.message(parentHandle);
  		for(String winHandle : driver.getWindowHandles())
  		{
  			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
  		}
  		SkySiteUtils.waitTill(5000);
  		driver.switchTo().defaultContent();
  		btntbPrjSetting.click();
  		Log.message("Setting button has been clicked");
  		SkySiteUtils.waitTill(5000); 	
  		Select drpcountry = new Select(driver.findElement(By.name("ddlProjectStatus")));
		drpcountry.selectByVisibleText("Complete");
		Log.message("Complete has been selected");
		SkySiteUtils.waitTill(5000);
		btnArchive.click();
		Log.message("Archive button has been clicked");
		SkySiteUtils.waitTill(3000);
		cheCollectionInformation.click();
		Log.message("check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnSave.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(80000);
		//driver.findElement(By.xpath("//*[@id='btnProcess']")).click();
		
		WebElement element = driver.findElement(By.xpath("//*[@id='btnProcess']"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
		
		SkySiteUtils.waitTill(3000);
		
		//btnMsgAlertClose.click();
		
		WebElement element1 = driver.findElement(By.xpath("//*[@id='btnMsgAlertClose']"));
	    JavascriptExecutor executor1 = (JavascriptExecutor)driver;
	    executor1.executeScript("arguments[0].click();", element1);
	    	
		Log.message("message pop up ok button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame   	
		btntitleStat.click();
		Log.message("title drop down button has been clicked");
		SkySiteUtils.waitTill(3000);
		btnancArchivedProjects.click();
		Log.message("Archive collection button has been clicked");
		SkySiteUtils.waitTill(30000);		
		btnResetSearch.click();
   		Log.message("reset button has been clicked");
   		SkySiteUtils.waitTill(3000);  
		int collec_chechboxCount1 = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
   		Log.message("collection chechbox Count is:" + collec_chechboxCount1);
   		// Loop start '2' as collection list start from tr[2]
   		int j = 0;
   		for (j = 1; j <= collec_chechboxCount1; j++)
   		{
   			// x path as dynamically providing all rows(i) tr[i]
   			String collName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[2]/a)["+j+"]")).getText();
   			Log.message(collName);
   			SkySiteUtils.waitTill(5000);   		
   			// Checking project name equal with each row of table
   			if (collName.equals(CollectionName)) 
   			{
   				Log.message("Select Collection edit button has been clicked!!!");
   				break;
   			}
   		}
   		//SkySiteUtils.waitTill(3000);   		
   		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
					.withTimeout(120, TimeUnit.SECONDS)//max wait 120 seconds
					.pollingEvery(1,TimeUnit.MILLISECONDS)//For every 1 seconds checks expected condition satisfies
					.ignoring(NoSuchElementException.class);//ignores if in between get exceptions
   		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//i[@class='icon icon-edit icon-orange'])["+j+"]")));//wait for element clickable								
   		SkySiteUtils.waitTill(3000); 
   		btnResetSearch.click();
   		Log.message("reset button has been clicked");
   		SkySiteUtils.waitTill(3000);  
   		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[12]/a/i)["+j+"]")).click();
   		Log.message("Edit button has been clicked");	
   		SkySiteUtils.waitTill(3000);  
   		String parentHandle1 = driver.getWindowHandle(); //Getting parent window
  		Log.message(parentHandle1);
  		for(String winHandle : driver.getWindowHandles())
  		{
  			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
  		}
  		driver.switchTo().defaultContent();
  		btntbPrjSetting.click();
  		Log.message("Setting button has been clicked");
  		SkySiteUtils.waitTill(5000); 
  		/*String collectionstatus=driver.findElement(By.xpath("//*[@id='ddlProjectStatus']")).getText();
  		Log.message("Collection Status is:"+collectionstatus);*/
  		if(btnClose.isDisplayed())  		
  			return true;  			  	
  		else
  			return false; 	  		
    }
    

    /** 
     * Method written for Verify Download in Archived collections
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Verify_Download_Archivedcollections(String CollectionName,String DownloadPath) throws AWTException
    {
    	boolean result1=true;
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().defaultContent();
    	btnCollections.click();
    	Log.message("collections button has been clicked");
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame   		
    	btnResetSearch.click();
    	Log.message("Reset button has been clicked");
    	SkySiteUtils.waitTill(2000);
    	int collec_chechboxCount = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
   		Log.message("collection chechbox Count is:" + collec_chechboxCount);
   		// Loop start '2' as collection list start from tr[2]
   		int i = 0;
   		for (i = 1; i <= collec_chechboxCount; i++)
   		{
   			// x path as dynamically providing all rows(i) tr[i]
   			String collName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[3]/a)["+i+"]")).getText();
   			Log.message(collName);
   			SkySiteUtils.waitTill(2000);   		
   			// Checking project name equal with each row of table
   			if (collName.equals(CollectionName)) 
   			{
   				Log.message("Select Collection edit button has been clicked!!!");
   				break;
   			}
   		}
   		SkySiteUtils.waitTill(3000);
   		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[11]/a)["+ i+"]")).click();
   		Log.message("Edit button has been clicked");
   		SkySiteUtils.waitTill(3000);
   		String parentHandle = driver.getWindowHandle(); //Getting parent window
  		Log.message(parentHandle);
  		for(String winHandle : driver.getWindowHandles())
  		{
  			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
  		}
  		SkySiteUtils.waitTill(3000);
  		driver.switchTo().defaultContent();
  		btntbPrjSetting.click();
  		Log.message("Setting button has been clicked");
  		SkySiteUtils.waitTill(5000); 	
  		Select drpcountry = new Select(driver.findElement(By.name("ddlProjectStatus")));
		drpcountry.selectByVisibleText("Complete");
		Log.message("Complete has been selected");
		SkySiteUtils.waitTill(5000);
		btnArchive.click();
		Log.message("Archive button has been clicked");
		SkySiteUtils.waitTill(3000);
		cheCollectionInformation.click();
		Log.message("check box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnSave.click();
		Log.message("save & close button has been clicked");
		SkySiteUtils.waitTill(80000);
		//driver.findElement(By.xpath("//*[@id='btnProcess']")).click();
		
		WebElement element = driver.findElement(By.xpath("//*[@id='btnProcess']"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
		
		SkySiteUtils.waitTill(3000);
		//btnMsgAlertClose.click();
		
		WebElement element1 = driver.findElement(By.xpath("//*[@id='btnMsgAlertClose']"));
	    JavascriptExecutor executor1 = (JavascriptExecutor)driver;
	    executor1.executeScript("arguments[0].click();", element1);
	    
		Log.message("message pop up ok button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().window(parentHandle);// Switch back to folder page
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame   	
		btntitleStat.click();
		Log.message("title drop down button has been clicked");
		SkySiteUtils.waitTill(3000);
		btnancArchivedProjects.click();
		Log.message("Archive collection button has been clicked");
		SkySiteUtils.waitTill(30000);		
		btnResetSearch.click();
   		Log.message("reset button has been clicked");
   		SkySiteUtils.waitTill(3000);  
		int collec_chechboxCount1 = driver.findElements(By.xpath("//*[@id='listGrid']/div[2]/table/tbody/tr/td[1]/img")).size();
   		Log.message("collection chechbox Count is:" + collec_chechboxCount1);
   		// Loop start '2' as collection list start from tr[2]
   		int j = 0;
   		for (j = 1; j <= collec_chechboxCount1; j++)
   		{
   			// x path as dynamically providing all rows(i) tr[i]
   			String collName = driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[2]/a)["+j+"]")).getText();
   			Log.message(collName);
   			SkySiteUtils.waitTill(2000);   		
   			// Checking project name equal with each row of table
   			if (collName.equals(CollectionName)) 
   			{
   				Log.message("Select Collection edit button has been clicked!!!");
   				break;
   			}
   		}
   		SkySiteUtils.waitTill(3000);   		
   		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
					.withTimeout(120, TimeUnit.SECONDS)//max wait 120 seconds
					.pollingEvery(1,TimeUnit.MILLISECONDS)//For every 1 seconds checks expected condition satisfies
					.ignoring(NoSuchElementException.class);//ignores if in between get exceptions
   		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//i[@class='icon icon-edit icon-orange'])["+j+"]")));//wait for element clickable								
   		btnResetSearch.click();
   		Log.message("reset button has been clicked");
   		SkySiteUtils.waitTill(3000); 
   		driver.switchTo().defaultContent();
   		fnaHomePage = new FnaHomePage(driver).get();
   		// Calling delete files from download folder script
   		fnaHomePage.Delete_Files_From_Folder(DownloadPath);
   		SkySiteUtils.waitTill(5000);  
   		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); // Back the Switch to Frame  		
   		driver.findElement(By.xpath("(//*[@id='listGrid']/div[2]/table/tbody/tr/td[9]/a/i)["+j+"]")).click();
   		Log.message("Download icon button has been clicked");	
   		SkySiteUtils.waitTill(5000);
   		driver.switchTo().frame(driver.findElement(By.id("ArchiveFrame"))); // Back the Switch to Frame   	
		String Coll_Foldername= driver.findElement(By.xpath("//*[@id='spnZipFileList']/ul/li[1]/div/div[1]")).getText();
		Log.message("Folder name is:"+Coll_Foldername);
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//*[@id='spnZipFileList']/ul/li[1]/div/div[2]/a")).click();
   		SkySiteUtils.waitTill(3000);
   		
   		// Get Browser name on run-time.
   		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
   		String browserName = caps.getBrowserName();
   		Log.message("Browser name on run-time is: " + browserName);
   		
   		if (browserName.contains("firefox"))
   		{
   			// Handling Download PopUp of firefox browser using robot
   			Robot robot = null;
   			robot = new Robot();   			
   			robot.keyPress(KeyEvent.VK_ALT);
   			SkySiteUtils.waitTill(2000);
   			robot.keyPress(KeyEvent.VK_S);
   			SkySiteUtils.waitTill(2000);
   			robot.keyRelease(KeyEvent.VK_ALT);
   			robot.keyRelease(KeyEvent.VK_S);
   			SkySiteUtils.waitTill(3000);
   			robot.keyPress(KeyEvent.VK_ENTER);
   			robot.keyRelease(KeyEvent.VK_ENTER);
   			SkySiteUtils.waitTill(20000);
   		}
   		SkySiteUtils.waitTill(25000);
   		// After checking whether download folder or not
   		String ActualFoldername = null;

   		File[] files = new File(DownloadPath).listFiles();
   		
   		for (File file : files)
   		{
   			if (file.isFile()) 
   			{
   				ActualFoldername = file.getName();// Getting Folder Name into a variable
   				Log.message("Actual Folder name is:" + ActualFoldername);
   				SkySiteUtils.waitTill(1000);
   				Long ActualFolderSize = file.length();
   				Log.message("Actual Folder size is:" + ActualFolderSize);
   				SkySiteUtils.waitTill(5000);
   				if (ActualFolderSize != 0) 
   				{
   					Log.message("Downloaded folder is available in downloads!!!");
   				} 
   				else
   				{
   					Log.message("Downloaded folder is NOT available in downloads!!!");
   				}
   			}
   		}
   		if (Coll_Foldername.trim().contentEquals(ActualFoldername))
   			return true;
   		else
   			return false;
  		  		
    }
    
}
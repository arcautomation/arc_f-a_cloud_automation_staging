package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class ActivityReportPage extends LoadableComponent<ActivityReportPage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	FnaHomePage fnaHomePage;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load()
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error
	{
		if (!isPageLoaded) 
		{
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public ActivityReportPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	 @FindBy(xpath = ".//*[@id='Collections']/a/i")
	 WebElement btnCollections;
	 
	 @FindBy(xpath = ".//*[@id='btnActivityReport']")
	 WebElement btnActivityReport;
	 
	 @FindBy(xpath = ".//*[@id='aspnetForm']/div[3]/nav/div[4]/div/ul/li[2]/a")
	 WebElement btnFileactivity;
	 
	 @FindBy(xpath = ".//*[@id='button1']")
	 WebElement btnview;
	
	/** 
	 * Method written for file activity report
	 * Scripted By: Sekhar      
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	public boolean File_ActivityReport(String DownloadPath,String DownloadPath1) throws AWTException, IOException 
	{	 	
	 	boolean result1=false;	
	 	// Calling delete files from download folder script		
	 	fnaHomePage = new FnaHomePage(driver).get();
	 	fnaHomePage.Delete_Files_From_Folder(DownloadPath);
	 	SkySiteUtils.waitTill(5000);
	 	btnCollections.click();
	 	Log.message("collection button has been clicked");
	 	SkySiteUtils.waitTill(3000); 
	 	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 	SkySiteUtils.waitTill(5000);
	 	btnActivityReport.click();
	 	Log.message("Activity button has been clicked");
	 	SkySiteUtils.waitTill(3000); 
	 	btnFileactivity.click();
	 	Log.message("File Activity button has been clicked");
	 	SkySiteUtils.waitTill(3000);
	 	String parentHandle1 = driver.getWindowHandle();
		Log.message(parentHandle1);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(8000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);
		
		Select drpcountry = new Select(driver.findElement(By.name("lstPrjList")));
		drpcountry.selectByVisibleText("All");
		Log.message("All has been selected");
		SkySiteUtils.waitTill(5000);		
		btnview.click();
		Log.message("View button has been clicked");
		SkySiteUtils.waitTill(15000);
		
		Select dropdown2 = new Select(driver.findElement(By.xpath("//select[@id='ReportViewer1_ctl01_ctl05_ctl00']")));//click on project dropdowm details 
		dropdown2.selectByVisibleText("Excel");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("//*[@id='ReportViewer1_ctl01_ctl05_ctl01']")).click();//click on export 
		SkySiteUtils.waitTill(5000);				
		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);
		    	
		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot1 = null;
			robot1 = new Robot();			
			robot1.keyPress(KeyEvent.VK_ALT);		
			robot1.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(4000);
			robot1.keyRelease(KeyEvent.VK_S);
			robot1.keyRelease(KeyEvent.VK_ALT);			
			SkySiteUtils.waitTill(3000);
			robot1.keyPress(KeyEvent.VK_ENTER);
			robot1.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
		SkySiteUtils.waitTill(25000);
		// After checking whether download file or not
		String expFilename = null;
		    	
		File[] files = new File(DownloadPath).listFiles();
		    	
		for(File file : files)
		{
			if(file.isFile()) 
			{
				expFilename=file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename);	
				Long ActualFileSize=file.length();
				Log.message("Actual File size is:"+ActualFileSize);
				SkySiteUtils.waitTill(1000);	
				if(ActualFileSize!=0)
				{
					result1=true;							
					break;
				}				
			}
		}
		if(result1==true)	
			return true;		
		else		
			return false;					
	}
	
	 @FindBy(xpath = ".//*[@id='aspnetForm']/div[3]/nav/div[4]/div/ul/li[3]/a")
	 WebElement btnuseractivity;
	 
	
	/** 
	 * Method written for user activity report
	 * Scripted By: Sekhar      
	 * @throws AWTException 
	 * @throws IOException 
	 */
	
	public boolean User_ActivityReport(String DownloadPath,String DownloadPath1) throws AWTException, IOException 
	{	 	
	 	boolean result1=false;	
	 	// Calling delete files from download folder script		
	 	fnaHomePage = new FnaHomePage(driver).get();
	 	fnaHomePage.Delete_Files_From_Folder(DownloadPath);
	 	SkySiteUtils.waitTill(5000);
	 	btnCollections.click();
	 	Log.message("collection button has been clicked");
	 	SkySiteUtils.waitTill(3000); 
	 	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
	 	SkySiteUtils.waitTill(5000);
	 	btnActivityReport.click();
	 	Log.message("Activity button has been clicked");
	 	SkySiteUtils.waitTill(3000); 
	 	btnuseractivity.click();
	 	Log.message("User Activity button has been clicked");
	 	SkySiteUtils.waitTill(3000); 
	 	String parentHandle1 = driver.getWindowHandle();
		Log.message(parentHandle1);
		for (String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's
													// your newly opened window)
		}
		SkySiteUtils.waitTill(8000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitTill(5000);		
		Select drpcountry = new Select(driver.findElement(By.name("lstPrjList")));
		drpcountry.selectByVisibleText("All");
		Log.message("All has been selected");
		SkySiteUtils.waitTill(5000);		
		btnview.click();
		Log.message("View button has been clicked");
		SkySiteUtils.waitTill(15000);
		
		Select dropdown2 = new Select(driver.findElement(By.xpath("//select[@id='ReportViewer1_ctl01_ctl05_ctl00']")));//click on project dropdowm details 
		dropdown2.selectByVisibleText("Excel");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("//*[@id='ReportViewer1_ctl01_ctl05_ctl01']")).click();//click on export 
		SkySiteUtils.waitTill(5000);				
		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);
		    	
		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot1 = null;
			robot1 = new Robot();			
			robot1.keyPress(KeyEvent.VK_ALT);		
			robot1.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(4000);
			robot1.keyRelease(KeyEvent.VK_S);
			robot1.keyRelease(KeyEvent.VK_ALT);			
			SkySiteUtils.waitTill(3000);
			robot1.keyPress(KeyEvent.VK_ENTER);
			robot1.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
		SkySiteUtils.waitTill(25000);
		// After checking whether download file or not
		String expFilename = null;
		    	
		File[] files = new File(DownloadPath).listFiles();
		    	
		for(File file : files)
		{
			if(file.isFile()) 
			{
				expFilename=file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:"+expFilename);	
				Long ActualFileSize=file.length();
				Log.message("Actual File size is:"+ActualFileSize);
				SkySiteUtils.waitTill(1000);	
				if(ActualFileSize!=0)
				{
					result1=true;							
					break;
				}				
			}
		}
		if(result1==true)	
			return true;		
		else		
			return false;					
	}
	
}
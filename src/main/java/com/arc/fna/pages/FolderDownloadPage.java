package com.arc.fna.pages;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.fna.pages.FnaHomePage;
import com.arc.fna.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class FolderDownloadPage extends LoadableComponent<FolderDownloadPage>
{

	WebDriver driver;
	private boolean isPageLoaded;	
	FnaHomePage fnaHomePage;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load()
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public FolderDownloadPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}	
	
	
	@FindBy(xpath = "//table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span")
	WebElement btn2ndfolder;
	
	@FindBy(xpath = "//*[@id='btnUpMove']")
	WebElement btnUpMove;
	
	@FindBy(xpath = "//table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span")
	WebElement btn1stfolder;
	
	@FindBy(xpath = ".//*[@id='btnDownMove']")
	WebElement btnDownMove;
	
	/** 
     * Method written for move up and move down
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Moveup_Movedown_Folderlevel()
    {          
    	SkySiteUtils.waitTill(5000);  
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	SkySiteUtils.waitTill(5000);
    	btn2ndfolder .click();
    	Log.message("2nd folder button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	String ActfolderName= btn2ndfolder.getText();
    	Log.message("Actual folder name is:"+ActfolderName);
    	SkySiteUtils.waitTill(2000);
    	btnUpMove.click();
    	Log.message("move up button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	String ExpfolderName= btn1stfolder.getText();
    	Log.message("Exp folder name is:"+ExpfolderName);    	
    	SkySiteUtils.waitTill(3000);
    	btnDownMove.click();
    	Log.message("move down button has been clicked");
    	SkySiteUtils.waitTill(3000);
    	String ExpfolderName1= btn2ndfolder.getText();
    	Log.message("Exp folder name1 is:"+ExpfolderName1);  
    	SkySiteUtils.waitTill(3000);
    	if(ActfolderName.equals(ExpfolderName)&&(ActfolderName.equals(ExpfolderName1)))
    		return true;
    	else
    		return false;
           
    }
    
    /**
	 * Method written for Random Folder Name Scripted By: Sekhar
	 * 
	 * @return
	 */

	public String Random_Foldername() 
	{

		String str = "Folder";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}
	
	@FindBy(xpath = "(//i[@class='icon icon-modify-folder'])[1]")
	WebElement fdrename;
	
	@FindBy(css = "#lftpnlMore")
	WebElement lftpnlMore;
	
	@FindBy(css = "#txtFolderName")
	WebElement txtFolderName;

	@FindBy(css = "#btnNewFolderSave")
	WebElement btnNewFolderSave;

	@FindBy(xpath = "(//span[@class='selectedTreeRow'])[1]")
	WebElement slttreefolder;
	
	@FindBy(xpath = "//table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[4]/span")
	WebElement slttreefolder1;
	

	/**
	 * Method written for Adding rename folder Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean Adding_Rename_Folder(String FolderName) throws AWTException
	{
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		// Switch to Frame
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Swich to I Frame Success!!");
		SkySiteUtils.waitTill(5000);
		slttreefolder1.click();
		Log.message("folder has been clicked");
		SkySiteUtils.waitTill(3000);
		lftpnlMore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		fdrename.click();
		Log.message("Rename folder has been clicked");
		SkySiteUtils.waitTill(3000);
		txtFolderName.clear();
		Log.message("folder name has been cleared");
		SkySiteUtils.waitTill(3000);
		txtFolderName.sendKeys(FolderName);
		Log.message("Folder name has been entered");
		SkySiteUtils.waitTill(3000);
		btnNewFolderSave.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(5000);
		String Folder_name = slttreefolder.getText();
		Log.message("Act Folder Name is:" + Folder_name);
		if (Folder_name.contains(FolderName))
		{
			Log.message("Adding rename folder validation successfull ");
			return true;
		} 
		else 
		{
			Log.message("Adding rename folder validation Unsuccessfull ");
			return false;
		}
	}
	
	// Deleting files from a folder
		public boolean Delete_Files_From_Folder(String Folder_Path) 
		{
			try
			{
				SkySiteUtils.waitTill(5000);
				Log.message("Cleaning download folder!!! ");
				File file = new File(Folder_Path);
				String[] myFiles;
				if (file.isDirectory()) 
				{
					myFiles = file.list();
					for (int i = 0; i < myFiles.length; i++) 
					{
						File myFile = new File(file, myFiles[i]);
						myFile.delete();
						SkySiteUtils.waitTill(5000);
					}
					Log.message("Available Folders/Files are deleted from download folder successfully!!!");
				}
			} // end try
			catch (Exception e) 
			{
				Log.message("Available Folders/Files are deleted from download folder successfully!!!");
			}
			return false;
		}
	
	@FindBy(xpath = "(//i[@class='icon icon-download'])[1]")
	WebElement fddownload;
	
	@FindBy(xpath = "(//tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span)[2]")
	WebElement folderlevel;

	/**
	 * Method written for Download Folder Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 * @throws IOException
	 */

	public boolean Download_Folder(String DownloadPath, String FolderName1) throws AWTException, IOException 
	{

		SkySiteUtils.waitTill(5000);
		folderlevel.click();
		SkySiteUtils.waitTill(3000);
		lftpnlMore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		// Calling delete files from download folder script
		this.Delete_Files_From_Folder(DownloadPath);
		SkySiteUtils.waitTill(5000);
		fddownload.click();
		Log.message("Download folder button has been clicked");
		SkySiteUtils.waitTill(8000);

		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);

		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(20000);
		}
		SkySiteUtils.waitTill(25000);
		// After checking whether download folder or not
		String ActualFoldername = null;

		File[] files = new File(DownloadPath).listFiles();

		for (File file : files)
		{
			if (file.isFile()) 
			{
				ActualFoldername = file.getName();// Getting Folder Name into a variable
				Log.message("Actual Folder name is:" + ActualFoldername);
				SkySiteUtils.waitTill(1000);
				Long ActualFolderSize = file.length();
				Log.message("Actual Folder size is:" + ActualFolderSize);
				SkySiteUtils.waitTill(5000);
				if (ActualFolderSize != 0) 
				{
					Log.message("Downloaded folder is available in downloads!!!");
				} 
				else
				{
					Log.message("Downloaded folder is NOT available in downloads!!!");
				}
			}
		}
		if (ActualFoldername.contains(FolderName1))
			return true;
		else
			return false;
	}
	
	@FindBy(xpath = "//*[@id='btnAddFolder']")
	WebElement btnAddFolder;
	
	@FindBy(xpath = "//*[@id='txtFolderName']")
	WebElement txtFolderName1;
	
	@FindBy(xpath = ".//*[@id='btnNewFolderSave']")
	WebElement btnNewFolderSave1;
	
	@FindBy(xpath = "//span[@class='noty_text']")
	WebElement noty_text;
	
	
	/**
	 * Method written for Adding rename folder 
	 * Scripted By: Sekhar
	 * @return
	 * @throws AWTException
	 */
	public boolean Same_Foldername(String FolderName) throws AWTException
	{
		SkySiteUtils.waitTill(3000);		
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		Log.message("Swich to I Frame Success!!");
		SkySiteUtils.waitTill(3000);
		btnAddFolder.click();
		Log.message("Create folder has been clicked");
		SkySiteUtils.waitTill(3000);
		txtFolderName1.sendKeys(FolderName);	
		Log.message("Folder name has been entered");
		SkySiteUtils.waitTill(3000);
		btnNewFolderSave1.click();
		Log.message("save button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		String notify = noty_text.getText();
		Log.message("notify msg is:"+notify);
		SkySiteUtils.waitTill(3000);
		if(notify.contains("Folder already exists."))
			return true;
		else
			return false;
	}
	
}
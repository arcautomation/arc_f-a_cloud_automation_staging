package com.arc.fna.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.arc.fna.utils.SkySiteUtils;

public class HomePage extends LoadableComponent<HomePage> {

	@Override
	protected void load() {}

	@Override
	protected void isLoaded() throws Error {}
	
	WebDriver driver;
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	@FindBy(how=How.ID, using="myFrame")
	WebElement iFrameModule;
	
	@FindBy(how=How.ID, using="Collections")
	WebElement collectionsTab;
	
	@FindBy(how=How.ID, using="ProjectFiles")
	WebElement documentsTab;
	
	public CollectionsPage openCollectionsTab() {
		if (!collectionsTab.getAttribute("class").equals("active")) {
			collectionsTab.click();
		}
		SkySiteUtils.waitForElement(driver, iFrameModule, 40);
		driver.switchTo().frame(iFrameModule);
		SkySiteUtils.waitTill(10);
		return new CollectionsPage(driver).get();
	}
	
	public DocumentsPage openDocumentsTab() {
		if (!documentsTab.getAttribute("class").equals("active")) {
			documentsTab.click();
		}
		SkySiteUtils.waitForElement(driver, iFrameModule, 40);
		driver.switchTo().frame(iFrameModule);
		SkySiteUtils.waitTill(10);
		return new DocumentsPage(driver).get();
	}
	
}